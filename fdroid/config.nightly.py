repo_url = "https://fuffle.net/nightly/repo"
repo_name = "Fuffle nightly repo"
repo_icon = "fdroid-icon.png"
repo_description = """
Fuffle nightly repo
"""

archive_older = 0

local_copy_dir = "/fdroid"

keystore = "keystore.jks"
