# Fuffle - Mastodon done... differently

> Fuffle(noun) - a group of bunnies *unofficial - term coined by users of social media*

Fuffle is a Flutter based client for Mastodon (and compatible services).

Fuffle is in a very rough but usable state, see it as an early alpha.

## Do not file any issues, complaints or whatever it is not ready until this warning is removed

Fuffle is still an unfinished project, despite me (the author) using it as my daily driver your experience may be horrible.
At this point the project is a fast moving target and may introduce breaking changes at any time. At this time in the project no user
configuration is provided and use may require some hacks due to unfixed bugs. 
Assume that all bugs you experience are known. Fuffle is a side project with zero commercial support.

* Testing mainly is done on an Android phone, consider any use in screen sizes larger than that impossible
* Linux is tested as it is developed on linux/arm64, some features left intentional broken
* iOS/macOS/Windows is not tested at all
* consider Web impossible at this moment

### Where to get (latest main CI build)

- [Android APK](https://gitlab.com/meyskens/fuffle/-/jobs/artifacts/main/raw/fuffle-1.0.0.apk?job=build%3Aapk)
- Android F-Droid "nightly" repository: https://fuffle.net/nightly/repo
- [Debian ARM](https://gitlab.com/meyskens/fuffle/-/jobs/artifacts/main/raw/fuffle_1.0.0_arm64.deb?job=build%3Adebian_aarch64)
- [Debian x86_64](https://gitlab.com/meyskens/fuffle/-/jobs/artifacts/main/raw/fuffle_1.0.0_amd64.deb?job=build%3Adebian_x86_64)

## About Fuffle

Fuffle is a very opinionated Mastodon client with a focus on:
* looking pretty
* being minimalist, no screen space wasted but still easy to navigate
* trying to be completionist compatible, infinite scrolling no "load more" bars stopping you, saving read state, not missing a single post
* being fediverse friendly, incorporating all aspects, not trying to just be commercial social media X or Y
* committed to open source and privacy
* focused on being functional, not addictive
This project is a LONG way away from that...

### Reasoning behind the project

There are many Mastodon clients on iOS however most in the Android world are forks of the official or resemble close to it.
My personal project was to bring the Mastodon experience I wanted to Android and Linux (so I could use a fully open source phone one day).
This client doesn't build on existing projects and tries to offer a different overall experience which can be found in the iOS ecosystem to other platforms.

### Credits

Fuffle is a fork of [fluffypix](https://github.com/krille-chan/fluffypix) however diverts a lot from the original application and vision. Thank you Krille and all contributors for giving a base project to start with!
