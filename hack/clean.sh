#!/bin/bash

dart analyze
dart format lib test
flutter pub get
flutter pub run import_sorter:main --no-comments
