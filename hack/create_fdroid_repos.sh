#!/usr/bin/env bash

GITLAB_PROJECT_ID="54594784"

# repo directory for build
mkdir fdroid/repo
# ... and for deployment
mkdir repo

git fetch

# building nightly repo

cd fdroid

cp config.nightly.py config.py

# get the current pipeline apk
JOB="$(curl https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines/$CI_PIPELINE_ID/jobs | jq -r '.[] | select(.name == "build:apk").id')"
URI="https://gitlab.com/meyskens/fuffle/-/jobs/$JOB/artifacts/raw/fuffle-$VERSION.apk"
FILENAME="fuffle-latest.apk"
wget --output-document="$FILENAME" "$URI"
mv "$FILENAME" repo

PIPELINES="$(curl https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines\?ref=main\&status=success\&order_by=updated_at | jq '.[].id' | head -n3)"

for PIPELINE in $PIPELINES
do
  JOB="$(curl https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines/$PIPELINE/jobs | jq -r '.[] | select(.name == "build:apk").id')"
  if [ -n $JOB ]; then
    URI="https://gitlab.com/meyskens/fuffle/-/jobs/$JOB/artifacts/raw/fuffle-$VERSION.apk"
    FILENAME="fuffle-$PIPELINE.apk"
    echo "Downloading $FILENAME from $URI ..."
    wget --output-document="$FILENAME" "$URI"
    mv "$FILENAME" repo
  fi
done

fdroid update --rename-apks
mkdir /fdroid && fdroid deploy
rm -rf /fdroid/archive
cd .. && mv -v /fdroid repo/nightly

# building stable + RC repo

rm -rf /fdroid fdroid/repo

mkdir fdroid/repo

cd fdroid
rm -f repo/*.apk

