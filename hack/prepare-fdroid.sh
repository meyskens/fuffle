#!/usr/bin/env bash

#cp -r android/fastlane fdroid/metadata/chat.fluffy.fluffychat
cd fdroid
echo "keypass=\"${FDROID_NIGHTLY_KEY_PASS}\"" >> config.nightly.py
echo "keystorepass=\"${FDROID_NIGHTLY_KEY_STORE_PASS}\"" >> config.nightly.py
echo "repo_keyalias=\"${FDROID_NIGHTLY_KEY_ALIAS}\"" >> config.nightly.py
chmod 600 config.nightly.py keystore.jks
cd ..
