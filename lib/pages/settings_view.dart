import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/utils/custom_about_dialog.dart';
import 'package:fuffle/utils/theme_mode_localization.dart';
import 'package:fuffle/widgets/core/theme_builder.dart';
import 'settings.dart';

class SettingsPageView extends StatelessWidget {
  final SettingsPageController controller;

  const SettingsPageView(this.controller, {super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(context)!.settings),
      ),
      body: ListView(
        controller: controller.scrollController,
        children: [
          ListTile(
            leading: const Icon(Icons.format_paint),
            title: Text(L10n.of(context)!.style),
            trailing: Text(ThemeController.of(context)
                .themeMode
                .toLocalizedString(context)),
            onTap: controller.setThemeMode,
          ),
          ListTile(
            leading: const Icon(Icons.color_lens),
            title: Text(L10n.of(context)!.color),
            trailing: ThemeController.of(context).themeMode == ThemeMode.system
                ? const Text("System Theme")
                : Icon(
                    Icons.circle,
                    color: ThemeController.of(context).primaryColor ??
                        AppConfigs.primaryColor,
                  ),
            onTap: controller.setColor,
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.person),
            title: Text(L10n.of(context)!.account),
            onTap: controller.settingsAction,
          ),
          const Divider(),
          SwitchListTile.adaptive(
            value: controller.useInAppBrowser,
            onChanged: controller.setUseInAppBrowser,
            title: Text(L10n.of(context)!.openLinksInAppBrowser),
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.privacy_tip),
            title: Text(L10n.of(context)!.privacy),
            onTap: controller.privacyAction,
          ),
          ListTile(
            leading: const Icon(Icons.info_outline),
            title: Text(L10n.of(context)!.about),
            onTap: () => showCustomAboutDialog(context),
          ),
          ListTile(
            leading: const Icon(Icons.question_mark),
            title: Text(L10n.of(context)!.help),
            onTap: controller.helpAction,
          ),
          const Divider(),
          ListTile(
            leading: controller.logoutLoading
                ? const CircularProgressIndicator()
                : const Icon(Icons.delete_forever_rounded),
            title: Text(L10n.of(context)!.logout),
            onTap: controller.logoutLoading ? null : controller.logout,
          ),
        ],
      ),
    );
  }
}
