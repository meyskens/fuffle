import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:emojis/emoji.dart' as emojis;
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:image_picker/image_picker.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status_visibility.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/widgets/content/text.dart';
import '../model/database/mastodon/status.dart';
import '../model/fuffle/api/api_extension.dart';
import '../model/fuffle/api/upload_extension.dart';
import 'compose_media_edit.dart';
import 'compose_view.dart';

class ComposePage extends StatefulWidget {
  final List<String>? atUsers;
  final Status? inReplyTo;
  final StatusVisibility? defaultVisibility;
  final String? contentWarning;

  final String? sharedText;
  final List<SharedMediaFile>? sharedMediaFiles;

  final FuffleAccount account;

  const ComposePage({
    super.key,
    required this.account,
    this.atUsers,
    this.defaultVisibility,
    this.sharedMediaFiles,
    this.sharedText,
    this.inReplyTo,
    this.contentWarning,
  });

  @override
  ComposePageController createState() => ComposePageController();
}

class ComposePageController extends State<ComposePage> {
  final TextEditingController cwController = TextEditingController();
  final TextEditingController statusController = TextEditingController();
  late StatusVisibility visibility;
  bool isContentWarned = false;

  late final List<Emoji> _instanceEmojis;
  late int maxStatusLength;

  List<ToUploadFile> media = [];

  bool loading = false;
  bool loadingPhoto = false;

  List<Map<String, String?>> getSuggestions(String text) {
    if (statusController.selection.baseOffset !=
            statusController.selection.extentOffset ||
        statusController.selection.baseOffset < 0) {
      return []; // no entries if there is selected text
    }
    final searchText = statusController.text
        .substring(0, statusController.selection.baseOffset);

    final List<Map<String, String?>> ret = <Map<String, String?>>[];
    const maxResults = 10;

    final emojiMatch = RegExp(r'(?:\s|^):([-\w]+)$').firstMatch(searchText);
    if (emojiMatch != null) {
      final emoteSearch = emojiMatch[1]!.toLowerCase();

      final matchingEmojis = _instanceEmojis
          .where((element) =>
              element.shortcode.toLowerCase().contains(emoteSearch) &&
              element.visibleInPicker)
          .toList();

      // sort by the index of the search term in the name in order to have
      // best matches first
      // (thanks for the hint by github.com/nextcloud/circles devs)
      matchingEmojis.sort((a, b) {
        final indexA = a.shortcode.indexOf(emoteSearch);
        final indexB = b.shortcode.indexOf(emoteSearch);
        if (indexA == -1 || indexB == -1) {
          if (indexA == indexB) return 0;
          if (indexA == -1) {
            return 1;
          } else {
            return 0;
          }
        }
        return indexA.compareTo(indexB);
      });

      for (var emoji in matchingEmojis) {
        ret.add({
          'type': 'instance_emoji',
          'name': emoji.shortcode,
          'url': emoji.url,
          'label': ':${emoji.shortcode}: - ${emoji.shortcode}',
          'current_word': emoteSearch,
        });

        if (ret.length > maxResults) {
          return ret;
        }
      }

      final matchingUnicodeEmojis = emojis.Emoji.all()
          .where(
            (element) => [element.name, ...element.keywords]
                .any((element) => element.toLowerCase().contains(emoteSearch)),
          )
          .toList();

      // sort by the index of the search term in the name in order to have
      // best matches first
      // (thanks for the hint by github.com/nextcloud/circles devs)
      matchingUnicodeEmojis.sort((a, b) {
        final indexA = a.name.indexOf(emoteSearch);
        final indexB = b.name.indexOf(emoteSearch);
        if (indexA == -1 || indexB == -1) {
          if (indexA == indexB) return 0;
          if (indexA == -1) {
            return 1;
          } else {
            return 0;
          }
        }
        return indexA.compareTo(indexB);
      });

      for (final emoji in matchingUnicodeEmojis) {
        ret.add({
          'type': 'emoji',
          'emoji': emoji.char,
          // don't include sub-group names, splitting at `:` hence
          'label': '${emoji.char} - ${emoji.name.split(':').first}',
          'current_word': ':$emoteSearch',
        });
        if (ret.length > maxResults) {
          return ret;
        }
      }
    }

    return ret;
  }

  Widget buildSuggestion(
    BuildContext context,
    Map<String, String?> suggestion,
  ) {
    const padding = EdgeInsets.all(4.0);
    if (suggestion['type'] == 'instance_emoji' ||
        suggestion['type'] == 'emoji') {
      final label = suggestion['label']!;
      return Tooltip(
        message: label,
        waitDuration: const Duration(days: 1), // don't show on hover
        child: Container(
          padding: padding,
          child: FuffleText(
            label,
            style: const TextStyle(fontFamily: 'monospace'),
            emojis: _instanceEmojis,
          ),
        ),
      );
    }

    return const SizedBox.shrink();
  }

  void insertSuggestion(Map<String, String?> suggestion) {
    final replaceText = statusController.text
        .substring(0, statusController.selection.baseOffset);
    var startText = '';
    final afterText = replaceText == statusController.text
        ? ''
        : statusController.text
            .substring(statusController.selection.baseOffset + 1);
    var insertText = '';

    if (suggestion['type'] == 'emoji') {
      insertText = '${suggestion['emoji']!} ';
      startText = replaceText.replaceAllMapped(
        suggestion['current_word']!,
        (Match m) => insertText,
      );
    }

    if (suggestion['type'] == 'instance_emoji') {
      insertText = ':${suggestion['name']!}: ';
      startText = replaceText.replaceFirstMapped(
        RegExp(':${suggestion['current_word']}\$'),
        (Match m) => insertText,
      );
    }

    if (insertText.isNotEmpty && startText.isNotEmpty) {
      statusController.text = startText + afterText;
      statusController.selection = TextSelection(
        baseOffset: startText.length,
        extentOffset: startText.length,
      );
    }
  }

  void addMedia() async {
    const maxSize = 2048.0;

    // TODO: fix pickMultiMedia() on Linux to select items
    final picks = await ImagePicker().pickMultiImage(
      maxHeight: maxSize,
      maxWidth: maxSize,
    );

    for (var pick in picks) {
      final bytes = await pick.readAsBytes();
      final isVideo =
          pick.mimeType != null && pick.mimeType!.indexOf("video/") == 0;

      setState(() => media.add(ToUploadFile(bytes, pick.name, video: isVideo)));
    }
  }

  void editMedia(ToUploadFile f) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => ComposeMediaEditPage(
          media: f,
          onChangeAlt: (alt) => setState(() => f.altText = alt),
        ),
      ),
    );
  }

  void removeMedia(ToUploadFile f) => setState(() => media.remove(f));

  @override
  void initState() {
    _init();
    super.initState();
  }

  void _init() async {
    final instance = widget.account.instance();
    _instanceEmojis = instance.emojis ?? [];
    maxStatusLength = instance.characterLimit ?? 500;

    statusController.clear();
    visibility = widget.defaultVisibility ??
        widget.account.preferences?.postingDefaultVisibility ??
        StatusVisibility.public;

    // in case this is a private account assume replies to be private as well
    if (widget.account.preferences?.postingDefaultVisibility ==
            StatusVisibility.private &&
        (visibility == StatusVisibility.public ||
            visibility == StatusVisibility.unlisted)) {
      visibility = StatusVisibility.private;
    }
    if (widget.atUsers != null) {
      for (var user in widget.atUsers!) {
        statusController.text += "@$user ";
      }
    }
    if (widget.sharedText != null) {
      statusController.text += widget.sharedText!;
    }
    if (widget.sharedMediaFiles != null) {
      setState(() => loadingPhoto = true);
      for (final sharedMediaFile in widget.sharedMediaFiles!) {
        try {
          final bytes = await File(sharedMediaFile.path).readAsBytes();
          setState(
            () => media.add(
              ToUploadFile(
                bytes,
                sharedMediaFile.path.split('/').last,
              ),
            ),
          );
        } catch (_) {
          if (mounted) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(L10n.of(context)!.oopsSomethingWentWrong),
              ),
            );
          }
        }
      }
      setState(() => loadingPhoto = false);
    }
    if (widget.contentWarning != null && widget.contentWarning!.isNotEmpty) {
      cwController.text = widget.contentWarning!;
      isContentWarned = true;
    }
  }

  void setVisibility() async {
    final newVisibility = await showModalActionSheet(
      context: context,
      title: L10n.of(context)!.visibility,
      message: visibility.toLocalizedString(context),
      actions: StatusVisibility.values
          .map(
            (vis) => SheetAction(
              key: vis,
              label: vis.toLocalizedString(context),
              icon: vis.icon,
              isDefaultAction: vis == visibility,
            ),
          )
          .toList(),
    );
    if (newVisibility == null) return;
    setState(() => visibility = newVisibility);
  }

  void postAction() async {
    if (!context.mounted) {
      return;
    }
    ProgressDialog pd = ProgressDialog(context: context);

    // TODO implement proper errors!
    if (statusController.text.isEmpty && media.isEmpty) {
      return;
    }
    setState(() => loading = true);

    try {
      final api = FuffleAPI(widget.account);
      final mediaIds = <String>[];

      if (media.isNotEmpty) {
        pd.show(
          max: media.length,
          msg: 'Media uploading...',
          progressBgColor: Colors.transparent,
          progressType: ProgressType.valuable,
        );

        pd.update(
          value: 0,
        );

        // TODO implement a train internet retry loop
        for (final file in media) {
          if (mounted) {
            final result = await api.upload(file.bytes, file.filename);
            mediaIds.add(result.id);

            if (file.altText != null) {
              await api.updateMediaAttachment(result.id,
                  description: file.altText);
            }

            pd.update(
                msg: 'Media uploading...', value: (media.indexOf(file) + 1));
          }
        }
        pd.close();
      }
      pd.show(
        msg: 'Posting...',
        progressBgColor: Colors.transparent,
      );
      if (mounted) {
        await api.publishNewStatus(
          status: statusController.text,
          sensitive: isContentWarned,
          spoiler: cwController.text,
          visibility: visibility,
          inReplyTo: widget.inReplyTo?.id,
          mediaIds: mediaIds,
        );
        pd.close();
      }

      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.newPostPublished),
          ),
        );

        context.pop();
      }
    } on ServerErrorResponse catch (error) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(error.toString()),
          ),
        );
        pd.close();
      }
      rethrow;
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
        pd.close();
      }
      rethrow;
    } finally {
      setState(() => loading = false);
    }
  }

  void toggleCW() => setState(() => isContentWarned = !isContentWarned);

  @override
  Widget build(BuildContext context) => ComposePageView(this);
}

class ToUploadFile {
  final Uint8List bytes;
  final String filename;
  final bool video;

  String? altText;

  ToUploadFile(this.bytes, this.filename, {this.video = false, this.altText});
}
