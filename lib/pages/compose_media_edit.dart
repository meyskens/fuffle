import 'package:flutter/material.dart';

import '../widgets/core/navbar.dart';
import 'compose_controller.dart';

class ComposeMediaEditPage extends StatefulWidget {
  final ToUploadFile media;
  final Function(String alt) onChangeAlt;

  const ComposeMediaEditPage({
    super.key,
    required this.media,
    required this.onChangeAlt,
  });

  @override
  ComposeMediaEditPageController createState() =>
      ComposeMediaEditPageController();
}

class ComposeMediaEditPageController extends State<ComposeMediaEditPage> {
  final TextEditingController altController = TextEditingController();

  @override
  void initState() {
    super.initState();
    altController.text = widget.media.altText ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return NavScaffold(
        floatingActionButton: Container(),
        hideNavigation: true,
        appBar: AppBar(
          title: const Text("Edit Media"),
        ),
        body: ListView(
          children: [
            SizedBox(
              height: 300,
              child: widget.media.video
                  ? Container()
                  : Image.memory(widget.media.bytes, fit: BoxFit.contain),
            ),
            const Divider(height: 10),
            TextField(
              minLines: 3,
              maxLines: 5,
              controller: altController,
              onChanged: (String value) => widget.onChangeAlt(value),
              autofocus: true,
              textCapitalization: TextCapitalization.sentences,
              decoration: const InputDecoration(
                hintText: "Explain what is on this image...",
                filled: true,
                contentPadding: EdgeInsets.all(12),
              ),
            ),
          ],
        ));
  }
}
