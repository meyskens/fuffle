import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_list_view/flutter_list_view.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'package:fuffle/widgets/core/account_switcher.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import 'package:fuffle/widgets/notificatons/notifications_widget.dart';
import 'notifications_controller.dart';

class NotificationsPageView extends StatelessWidget {
  final NotificationsPageController controller;

  const NotificationsPageView(this.controller, {super.key});
  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      currentIndex: 3,
      appBar: AppBar(
        leading: AccountSwitcher(
          account: controller.widget.account,
        ),
        title: const Text("Notifications"),
        actions: const [],
      ),
      body: RefreshIndicator(
        onRefresh: controller.refresh,
        child: Stack(
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: FlutterListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                controller: controller.timelineController.listViewController,
                delegate: FlutterListViewDelegate(
                  (context, i) => VisibilityDetector(
                    key: Key(i.toString()),
                    onVisibilityChanged: (visibilityInfo) {
                      if (visibilityInfo.visibleFraction > 0.5) {
                        controller.timelineController.markRead(i);
                      }
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      key: ValueKey(
                          controller.timelineController.uiTimeline[i].id),
                      children: [
                        NotificationWidget(
                          key: ValueKey(
                              controller.timelineController.uiTimeline[i].id),
                          notification:
                              controller.timelineController.uiTimeline[i],
                        ),
                      ],
                    ),
                  ),
                  childCount: controller.timelineController.uiTimeline.length,
                  keepPosition: true,
                  keepPositionOffset: 0,
                  initIndex: controller.initIndex,
                  onItemKey: (i) =>
                      controller.timelineController.uiTimeline[i].id,
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: Chip(
                label: Text(
                  controller.timelineController.unreadCount.toString(),
                ),
                padding: EdgeInsets.zero,
                backgroundColor: Theme.of(context).secondaryHeaderColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: const BorderSide(color: Colors.transparent)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
