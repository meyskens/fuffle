import 'package:flutter/material.dart';

import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import '../model/database/mastodon/status_visibility.dart';
import 'compose_controller.dart';

class ComposePageView extends StatelessWidget {
  final ComposePageController controller;
  const ComposePageView(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    const buttonPad = EdgeInsets.only(left: 4, right: 4);
    return NavScaffold(
      floatingActionButton: Container(),
      hideNavigation: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(controller.visibility == StatusVisibility.direct
            ? L10n.of(context)!.newMessage
            : L10n.of(context)!.newStatus),
        actions: [
          TextButton(
            onPressed: controller.loading || controller.loadingPhoto
                ? null
                : controller.postAction,
            child: Row(
              children: [
                Text(L10n.of(context)!.post),
                const SizedBox(width: 8),
                const Icon(Icons.send_outlined),
              ],
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.all(12),
            children: [
              if (controller.isContentWarned)
                TextField(
                  minLines: 1,
                  maxLines: 1,
                  textCapitalization: TextCapitalization.sentences,
                  controller: controller.cwController,
                  decoration: const InputDecoration(
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(right: 8),
                      child: Icon(Icons.warning_rounded),
                    ),
                    hintText: "Content Warning",
                    filled: false,
                    contentPadding: EdgeInsets.all(12),
                  ),
                ),
              //SizedBox(height: 16,),
              TypeAheadField<Map<String, String?>>(
                direction: VerticalDirection.down,
                hideOnEmpty: true,
                hideOnLoading: true,
                controller: controller.statusController,
                // focusNode: focusNode,
                hideOnSelect: false,
                debounceDuration: const Duration(milliseconds: 50),
                suggestionsCallback: controller.getSuggestions,
                itemBuilder: controller.buildSuggestion,
                onSelected: controller.insertSuggestion,
                errorBuilder: (BuildContext context, Object? error) =>
                    const SizedBox.shrink(),
                loadingBuilder: (BuildContext context) =>
                    const SizedBox.shrink(),
                // fix loading briefly flickering a dark box
                emptyBuilder: (BuildContext context) => const SizedBox
                    .shrink(), // fix loading briefly showing no suggestions

                builder: (context, textController, focusNode) => TextField(
                  minLines: 4,
                  maxLines: 8,
                  maxLength: controller.maxStatusLength,
                  autofocus: true,
                  textCapitalization: TextCapitalization.sentences,
                  controller: textController,
                  focusNode: focusNode,
                  decoration: const InputDecoration(
                    hintText: "Tell me more...",
                    filled: true,
                    contentPadding: EdgeInsets.all(12),
                  ),
                ),
              ),
              const SizedBox(height: 12),
              if (controller.media.isNotEmpty)
                GridView.count(
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  children: controller.media
                      .map((e) => _PickedImage(controller, e))
                      .toList(),
                )
            ],
          ),
          Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                color: Color.lerp(
                    Theme.of(context).colorScheme.secondary, Colors.white, 0.8),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        onPressed: controller.media.length < 4
                            ? controller.addMedia
                            : null,
                        disabledColor: Colors.grey,
                        icon: const Padding(
                          padding: buttonPad,
                          child: Icon(Icons.image),
                        ),
                      ),
                      IconButton(
                          onPressed: controller.toggleCW,
                          icon: const Padding(
                            padding: buttonPad,
                            child: Icon(Icons.warning_rounded),
                          )),
                      IconButton(
                          onPressed: controller.setVisibility,
                          icon: Padding(
                            padding: buttonPad,
                            child: Icon(controller.visibility.icon),
                          )),
                      IconButton(
                          onPressed: () => {},
                          icon: const Padding(
                            padding: buttonPad,
                            child: Icon(Icons.poll),
                          )),
                      IconButton(
                          onPressed: () => {},
                          icon: const Padding(
                            padding: buttonPad,
                            child: Row(
                              children: [Icon(Icons.translate), Text("EN")],
                            ),
                          )),
                    ],
                  ),
                ),
              )),
        ],
      ),
      currentIndex: 2,
    );
  }
}

class _PickedImage extends StatelessWidget {
  final ComposePageController controller;
  final ToUploadFile media;
  const _PickedImage(this.controller, this.media);

  String format(double bytes) {
    final dictionary = [
      "bytes",
      "KB",
      "MB",
      "GB",
      "TB",
      "PB",
      "EB",
      "ZB",
      "YB"
    ];
    int index = 0;
    for (index = 0; index < dictionary.length; index++) {
      if (bytes < 1024) {
        break;
      }
      bytes = bytes / 1024;
    }
    return '${(bytes * 100).round() / 100} ${dictionary[index]}';
  }

  @override
  Widget build(BuildContext context) {
    const br = BorderRadius.all(Radius.circular(15));
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 2,
        borderRadius: br,
        child: ClipRRect(
          borderRadius: br,
          child: InkWell(
            onTap: () => controller.editMedia(media),
            child: Stack(
              children: [
                media.video
                    ? Container(
                        height: 256,
                        color: Theme.of(context).dividerColor,
                        child: Stack(
                          children: [
                            const BlurHash(hash: AppConfigs.fallbackBlurHash),
                            Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.video_camera_back,
                                    size: 56,
                                  ),
                                  Text(format(media.bytes.length.toDouble())),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    : Image.memory(
                        media.bytes,
                        fit: BoxFit.cover,
                        height: double.infinity,
                        width: double.infinity,
                      ),
                Positioned(
                  top: 10,
                  right: -20,
                  child: MaterialButton(
                    onPressed: () => controller.removeMedia(media),
                    shape: const CircleBorder(),
                    color: Colors.white,
                    textColor: Colors.grey,
                    child: const Icon(Icons.close),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: media.altText == null
                      ? const Icon(Icons.textsms_outlined)
                      : const Icon(Icons.textsms, color: Colors.green),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
