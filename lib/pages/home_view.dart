import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_list_view/flutter_list_view.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/widgets/core/account_switcher.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import 'package:fuffle/widgets/status/status_widget.dart';
import 'home_controller.dart';

class HomePageView extends StatelessWidget {
  final HomePageController controller;

  const HomePageView(this.controller, {super.key});
  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      appBar: AppBar(
        leading: AccountSwitcher(
          account: controller.widget.account,
        ),
        title: const Text(AppConfigs.applicationName),
        actions: const [],
      ),
      body: RefreshIndicator(
        onRefresh: controller.refresh,
        child: Stack(
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: FlutterListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                controller: controller.timelineController.listViewController,
                delegate: FlutterListViewDelegate(
                  (BuildContext context, int i) => VisibilityDetector(
                    key: ValueKey(
                        controller.timelineController.uiTimeline[i].id),
                    onVisibilityChanged: (visibilityInfo) {
                      if (visibilityInfo.visibleFraction > 0.5) {
                        controller.timelineController.markRead(i);
                      }
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      key: ValueKey(
                          controller.timelineController.uiTimeline[i].id),
                      children: [
                        StatusWidget(
                          ownAccount: controller.widget.account,
                          key: ValueKey(
                              controller.timelineController.uiTimeline[i].id),
                          status: controller.timelineController.uiTimeline[i],
                          onUpdate: controller.onUpdateStatus,
                        ),
                      ],
                    ),
                  ),
                  childCount: controller.timelineController.uiTimeline.length,
                  keepPosition: true,
                  keepPositionOffset: 0,
                  onItemKey: (i) =>
                      controller.timelineController.uiTimeline[i].id,
                  initIndex: controller.initIndex,
                  firstItemAlign: FirstItemAlign.start,
                  // onIsPermanent: (_) => true,
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: controller.timelineController.unreadCount > 0
                  ? InkWell(
                      onDoubleTap: controller.timelineController.scrollToTop,
                      child: Chip(
                        label: Text(
                          controller.timelineController.unreadCount.toString(),
                        ),
                        padding: EdgeInsets.zero,
                        backgroundColor: Theme.of(context).secondaryHeaderColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: const BorderSide(color: Colors.transparent)),
                      ),
                    )
                  : const SizedBox.shrink(),
            ),
          ],
        ),
      ),
    );
  }
}
