import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/pages/home_view.dart';
import 'package:fuffle/utils/home_timeline_controller.dart';

class HomePage extends StatefulWidget {
  final FuffleAccount account;

  const HomePage({super.key, required this.account});

  @override
  HomePageController createState() => HomePageController();
}

class HomePageController extends State<HomePage> {
  bool _initDone = false;

  Timer? _updateTimer;

  late final HomeTimelineController timelineController;
  late final int initIndex;

  Future<void> refresh({bool fetch = true}) async {
    await timelineController.init();
    if (!_initDone) {
      initIndex = timelineController.readIndex;
      _initDone = true;
      setState(() {});
    }

    if (mounted) {
      try {
        if (fetch) {
          final cnt = await timelineController.fetch();
          if (cnt > 0) {
            timelineController.controllerTLtoUITL();
            return refresh(fetch: fetch);
          }
        } else {
          timelineController.controllerTLtoUITL();
        }
      } catch (e) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(e.toString()),
            ),
          );
        }
      }
    }
  }

  void onUpdateStatus(Status? status, [String? statusID]) {
    if (status == null && statusID != null) {
      timelineController.removeID(statusID);
      timelineController.controllerTLtoUITL();
      return;
    } else if (status == null) {
      return;
    }

    timelineController.updateStatus(statusID ?? status.id, status);
    timelineController.controllerTLtoUITL();
  }

  void settingsAction() => context.push('/settings');
  void goToHashtag(String tag) => context.push('/tags/$tag');
  void goToMessages() => context.push('/messages');
  void goToUser(String id) => context.push('/user/$id');

  // Sharing logic
  // no idea what this is doing here
  // should be moved upstream
  StreamSubscription? _intentTextStreamSubscription;
  StreamSubscription? _intentFileStreamSubscription;

  void _initReceiveSharingIntent() {
    // For sharing images coming from outside the app while the app is in the memory
    _intentFileStreamSubscription = ReceiveSharingIntent.getMediaStream()
        .listen((List<SharedMediaFile> value) {
      if (value.isEmpty) return;
      context.push('/sharemedia');
    });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then((List<SharedMediaFile> value) {
      if (value.isEmpty) return;
      context.push('/sharemedia');
    });

    // TODO: fix text sharing
  }

  @override
  void initState() {
    timelineController = HomeTimelineController(context,
        setState: setState, account: widget.account);

    refresh(fetch: false).whenComplete(() => refresh(fetch: true));

    if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
      _initReceiveSharingIntent();
    }

    super.initState();

    startTimer();
  }

  void startTimer() {
    if (_updateTimer == null || !_updateTimer!.isActive) {
      _updateTimer = Timer.periodic(const Duration(seconds: 30), (timer) {
        if (mounted && !Fuffle.of(context).isInBackground) {
          refresh();
        }
      });
    }
  }

  @override
  void activate() {
    startTimer();
    super.activate();
  }

  @override
  void deactivate() {
    _updateTimer?.cancel();
    super.deactivate();
  }

  @override
  void dispose() {
    _intentTextStreamSubscription?.cancel();
    _intentFileStreamSubscription?.cancel();

    _updateTimer?.cancel();

    timelineController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_initDone) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return HomePageView(this);
  }
}
