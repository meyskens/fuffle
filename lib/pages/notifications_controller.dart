import 'dart:async';

import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/pages/notifications_view.dart';
import 'package:fuffle/utils/notifications_timeline_controller.dart';

class NotificationsPage extends StatefulWidget {
  final FuffleAccount account;

  const NotificationsPage({super.key, required this.account});
  @override
  NotificationsPageController createState() => NotificationsPageController();
}

class NotificationsPageController extends State<NotificationsPage> {
  bool _initDone = false;
  late final int initIndex;

  Timer? _updateTimer;

  late final NotificationsTimelineController timelineController;

  Future<void> refresh({bool fetch = true}) async {
    if (!_initDone) {
      await timelineController.init();
      initIndex = timelineController.readIndex;
      timelineController.controllerTLtoUITL();
      _initDone = true;

      setState(() {});
    }
    if (mounted) {
      try {
        if (fetch) {
          final cnt = await timelineController.fetch();
          if (cnt > 0) {
            timelineController.controllerTLtoUITL();
            return refresh();
          }
        } else {
          timelineController.controllerTLtoUITL();
        }
      } catch (e) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(e.toString()),
            ),
          );
          rethrow;
        }
      }
    }
  }

  @override
  void initState() {
    timelineController = NotificationsTimelineController(context,
        setState: setState, account: widget.account);

    refresh(fetch: false).whenComplete(() => refresh());

    super.initState();

    startTimer();
  }

  void startTimer() {
    if (_updateTimer == null || !_updateTimer!.isActive) {
      _updateTimer = Timer.periodic(const Duration(seconds: 60), (timer) {
        if (mounted && !Fuffle.of(context).isInBackground) {
          refresh();
        }
      });
    }
  }

  @override
  void activate() {
    startTimer();
    super.activate();
  }

  @override
  void deactivate() {
    _updateTimer?.cancel();
    super.deactivate();
  }

  @override
  void dispose() {
    _updateTimer?.cancel();

    timelineController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_initDone) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return NotificationsPageView(this);
  }
}
