import 'dart:async';

import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/mastodon/search_result.dart';
import '../model/fuffle/api/api_extension.dart';
import 'views/search_view.dart';

// ignore: unused_import

class SearchPage extends StatefulWidget {
  final FuffleAccount account;

  const SearchPage({super.key, required this.account});
  @override
  SearchPageController createState() => SearchPageController();
}

class SearchPageController extends State<SearchPage> {
  late final FuffleAPI _api = FuffleAPI(widget.account);
  final ScrollPhysics scrollPhysics = const ScrollPhysics();
  List<Status> timeline = [];
  final refreshController = RefreshController(initialRefresh: false);
  final TextEditingController textEditingController = TextEditingController();
  final scrollController = ScrollController();

  SearchResult? searchResult;

  bool loading = false;

  Timer? _cooldown;

  void searchQueryWithCooldown([_]) {
    _cooldown?.cancel();
    _cooldown = Timer(const Duration(milliseconds: 500), searchQuery);
  }

  void cancelSearch() {
    textEditingController.clear();
    searchQuery();
  }

  void searchQuery([_]) async {
    _cooldown?.cancel();
    if (textEditingController.text.isEmpty) {
      setState(() {
        searchResult = null;
        loading = false;
      });
      return;
    }
    final query = textEditingController.text.toLowerCase().trim();
    setState(() {
      loading = true;
    });
    try {
      final result = await _api.search(query);

      if (result.statuses.isEmpty) {
        result.statuses.addAll(
          timeline.where(
            (status) =>
                status.content != null &&
                status.content!.toLowerCase().contains(query),
          ),
        );
        if (result.hashtags.isNotEmpty && result.hashtags.first.name == query) {
          final statuses = await _api.requestTagTimeline(query);
          result.statuses.addAll(statuses);
        }
      }
      setState(() {
        searchResult = result;
      });
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  void goToHashtag(String tag) => context.push('/tags/$tag');
  void goToUser(String id) => context.push('/user/$id');

  void onUpdateStatus(Status? status, [String? deleteId]) {
    if (status == null) {
      setState(() {
        searchResult?.statuses.removeWhere((s) => s.id == deleteId);
      });
      return;
    }
    setState(() {
      searchResult?.statuses[searchResult!.statuses.indexWhere(
          (s) => s.id == status.id || s.reblog?.id == status.id)] = status;
    });
  }

  bool get useDiscoverGridView => false;
  void setUseDiscoverGridView(bool? b) {
    if (b == null) return;
    setState(() {
      // Fuffle.of(context).useDiscoverGridView = b;
    });
    refreshController.requestRefresh();
  }

  void refresh() async {
    if (textEditingController.text.isNotEmpty) {
      return searchQuery();
    }
    try {
      timeline = await _api.requestPublicTimeline(
        mediaOnly: useDiscoverGridView,
        local: true,
      );
      setState(() {});
      if (mounted) {
        refreshController.refreshCompleted();
      }
    } catch (_) {
      if (mounted) {
        setState(() {});
        refreshController.refreshFailed();
        if (timeline.isEmpty) {
          Timer(const Duration(seconds: 3), refreshController.requestRefresh);
        }
      }
      rethrow;
    }
  }

  void loadMore() async {
    try {
      final statuses = await _api.requestPublicTimeline(
        maxId: timeline.last.id,
        mediaOnly: useDiscoverGridView,
        local: true,
      );
      timeline.addAll(statuses);
      setState(() {});
      refreshController.loadComplete();
    } catch (_) {
      refreshController.loadFailed();
      rethrow;
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refreshController.requestRefresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) => SearchPageView(this);
}
