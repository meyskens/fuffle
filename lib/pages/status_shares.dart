import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'views/status_shares_view.dart';

class StatusSharesPage extends StatefulWidget {
  final String statusId;
  final FuffleAccount account;
  const StatusSharesPage(
      {required this.statusId, required this.account, super.key});

  @override
  StatusSharesPageController createState() => StatusSharesPageController();
}

class StatusSharesPageController extends State<StatusSharesPage> {
  late final Future<List<Account>> request;
  @override
  void initState() {
    super.initState();
    request = FuffleAPI(widget.account).statusRebloggedBy(widget.statusId);
  }

  @override
  Widget build(BuildContext context) => StatusSharesPageView(this);
}
