import 'dart:async';

import 'package:flutter/material.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/utils/mentions_timeline_controller.dart';
import 'mentions_view.dart';

class MentionsPage extends StatefulWidget {
  final FuffleAccount account;

  const MentionsPage({super.key, required this.account});
  @override
  MentionsPageController createState() => MentionsPageController();
}

class MentionsPageController extends State<MentionsPage> {
  bool _initDone = false;

  Timer? _updateTimer;

  late final MentionsTimelineController timelineController;
  late final int initIndex;

  Future<void> refresh({bool fetch = true}) async {
    await timelineController.init();
    if (!_initDone) {
      initIndex = timelineController.readIndex;
      _initDone = true;
      setState(() {});
    }
    if (mounted) {
      try {
        if (fetch) {
          final cnt = await timelineController.fetch();
          if (cnt > 0) {
            timelineController.controllerTLtoUITL();
            return refresh(fetch: fetch);
          }
        } else {
          timelineController.controllerTLtoUITL();
        }
      } catch (e) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(e.toString()),
            ),
          );
        }
      }
    }
  }

  void onUpdateStatus(Status? status, [String? statusID]) {
    if (status == null && statusID != null) {
      timelineController.removeID(statusID);
      timelineController.controllerTLtoUITL();
      return;
    } else if (status == null) {
      return;
    }

    timelineController.updateStatus(statusID ?? status.id, status);
    timelineController.controllerTLtoUITL();
  }

  void goToHashtag(String tag) => context.push('/tags/$tag');
  void goToMessages() => context.push('/messages');
  void goToUser(String id) => context.push('/user/$id');

  @override
  void initState() {
    timelineController = MentionsTimelineController(context,
        setState: setState, account: widget.account);

    refresh(fetch: false).whenComplete(() => refresh(fetch: true));

    super.initState();

    startTimer();
  }

  void startTimer() {
    if (_updateTimer == null || !_updateTimer!.isActive) {
      _updateTimer = Timer.periodic(const Duration(seconds: 30), (timer) {
        if (mounted && !Fuffle.of(context).isInBackground) {
          refresh();
        }
      });
    }
  }

  @override
  void activate() {
    startTimer();
    super.activate();
  }

  @override
  void deactivate() {
    _updateTimer?.cancel();
    super.deactivate();
  }

  @override
  void dispose() {
    _updateTimer?.cancel();

    timelineController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_initDone) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return MentionsPageView(this);
  }
}
