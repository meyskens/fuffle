import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'views/status_likes_view.dart';

class StatusLikesPage extends StatefulWidget {
  final String statusId;
  final FuffleAccount account;
  const StatusLikesPage(
      {required this.statusId, required this.account, super.key});

  @override
  StatusLikesPageController createState() => StatusLikesPageController();
}

class StatusLikesPageController extends State<StatusLikesPage> {
  late final Future<List<Account>> request;
  @override
  void initState() {
    super.initState();
    request = FuffleAPI(widget.account).statusFavouritedBy(widget.statusId);
  }

  @override
  Widget build(BuildContext context) => StatusLikesPageView(this);
}
