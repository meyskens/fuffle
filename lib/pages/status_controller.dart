import 'package:flutter/material.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/mastodon/status_context.dart';
import '../model/fuffle/api/api_extension.dart';
import 'status_view.dart';

class StatusPage extends StatefulWidget {
  final String statusId;
  final Status? status;
  final FuffleAccount account;
  const StatusPage({
    required this.statusId,
    required this.account,
    this.status,
    super.key,
  });

  @override
  StatusPageController createState() => StatusPageController();
}

class StatusPageController extends State<StatusPage> {
  final ScrollPhysics scrollPhysics = const ScrollPhysics();
  Status? status;
  StatusContext? statusContext;

  bool commentLoading = false;

  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  // final ScrollController scrollController = ScrollController();
  //final FocusNode focusNode = FocusNode();

  void refresh() async {
    try {
      final api = FuffleAPI(widget.account);
      status ??= await api.getStatus(widget.statusId);
      statusContext = await api.getStatusContext(widget.statusId);
      setState(() {});
      refreshController.refreshCompleted();
    } catch (_) {
      refreshController.refreshFailed();
      rethrow;
    }
  }

  void onUpdateStatus(Status? status, [String? deleteId]) {
    if (status == null) {
      if (deleteId == this.status?.id) {
        context.pop((route) => route.isFirst);
        return;
      }
      setState(() {
        statusContext?.ancestors.removeWhere((s) => s.id == deleteId);
        statusContext?.descendants.removeWhere((s) => s.id == deleteId);
      });
      return;
    }
    setState(() {
      if (this.status?.id == status.id ||
          this.status?.reblog?.id == status.id) {
        this.status = status;
      } else {
        final ancestorIndex = statusContext!.ancestors
            .indexWhere((s) => s.id == status.id || s.reblog?.id == status.id);
        if (ancestorIndex != -1) {
          statusContext?.ancestors[ancestorIndex] = status;
        } else {
          final descendantIndex = statusContext!.descendants.indexWhere(
              (s) => s.id == status.id || s.reblog?.id == status.id);
          if (descendantIndex != -1) {
            statusContext?.descendants[descendantIndex] = status;
          }
        }
      }
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refreshController.requestRefresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) => StatusPageView(this);
}
