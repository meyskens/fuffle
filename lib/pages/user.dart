import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'package:fuffle/model/mastodon/relationships.dart';
import 'package:fuffle/utils/links_callback.dart';
import '../model/fuffle/api/api_extension.dart';
import 'views/user_view.dart';

class UserPage extends StatefulWidget {
  final String id;
  final FuffleAccount account;

  const UserPage({required this.id, required this.account, super.key});

  @override
  UserPageController createState() => UserPageController();
}

class UserPageController extends State<UserPage> {
  final ScrollPhysics scrollPhysics = const ScrollPhysics();
  Account? account;
  bool get isOwnUser => widget.account.mastodonAccount()!.id == widget.id;

  List<Status>? timeline;
  List<Account>? followers;
  List<Account>? following;
  Relationships? relationships;

  String? nextFollowers;
  String? nextFollowing;

  bool loadFollowChanges = false;

  final refreshController = RefreshController(initialRefresh: false);
  final scrollController = ScrollController();

  void refresh() async {
    try {
      final api = FuffleAPI(widget.account);
      account ??= await api.loadAccount(widget.id);

      relationships ??= await api.getRelationship(widget.id);

      timeline = await api.requestUserTimeline(
        widget.id,
        onlyMedia: false,
      );

      setState(() {});
      refreshController.refreshCompleted();
    } catch (_) {
      refreshController.refreshFailed();
      if (timeline == null) {
        Timer(const Duration(seconds: 3), refreshController.requestRefresh);
      }
      rethrow;
    }
  }

  void onUpdateStatus(Status? status, [String? deleteId]) {
    if (status == null) {
      setState(() {
        timeline!.removeWhere((s) => s.id == deleteId);
      });
      return;
    }
    final index = timeline!
        .indexWhere((s) => s.id == status.id || s.reblog?.id == status.id);
    if (index == -1) {
      refreshController.requestRefresh();
    } else {
      setState(() {
        timeline![index] = status;
      });
    }
  }

  void loadMore() async {
    try {
      final statuses = await FuffleAPI(widget.account).requestUserTimeline(
        widget.id,
        maxId: timeline!.last.id,
        excludeReplies: false,
        onlyMedia: false,
      );
      timeline!.addAll(statuses);

      setState(() {});
      refreshController.loadComplete();
    } catch (_) {
      refreshController.loadFailed();
      rethrow;
    }
  }

  void onPopupAction(PopupActions action) async {
    final api = FuffleAPI(widget.account);

    final snackBar = ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(L10n.of(context)!.loading),
        duration: const Duration(seconds: 30),
      ),
    );
    try {
      late final Relationships newRelationships;
      switch (action) {
        case PopupActions.block:
          if ((relationships?.blocking ?? false)) {
            newRelationships = await api.unblock(account!.id);
          } else {
            newRelationships = await api.block(account!.id);
          }
          break;
        case PopupActions.mute:
          if ((relationships?.muting ?? false)) {
            newRelationships = await api.unmute(account!.id);
          } else {
            newRelationships = await api.mute(account!.id);
          }
          break;
        case PopupActions.website:
          linksCallback(account!.url, context);
          break;
        case PopupActions.share:
          if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
            Share.share(account!.url);
          } else {
            Clipboard.setData(ClipboardData(text: account!.url));
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(L10n.of(context)!.copiedToClipboard),
              ),
            );
          }
          break;
      }
      snackBar.close();
      setState(() {
        relationships = newRelationships;
      });
    } catch (_) {
      snackBar.close();
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    }
  }

  void follow() => _setFollowStatus(true);
  void unfollow() => _setFollowStatus(false);

  void _setFollowStatus(bool follow) async {
    setState(() {
      loadFollowChanges = true;
    });
    try {
      final api = FuffleAPI(widget.account);
      final newRelationships =
          follow ? await api.follow(widget.id) : await api.unfollow(widget.id);
      setState(() {
        relationships = newRelationships;
      });
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    } finally {
      setState(() {
        loadFollowChanges = false;
      });
    }
  }

  void goToSettings() => context.push('/settings');

  void goToProfile(String id) => context.push('/user/$id');

  void sendMessage() => context.push('/compose');

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refreshController.requestRefresh();
    });
  }

  @override
  Widget build(BuildContext context) => UserPageView(this);
}
