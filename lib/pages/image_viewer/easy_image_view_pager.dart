import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'easy_image_provider.dart';
import 'easy_image_view.dart';

/// Custom ScrollBehavior that allows dragging with all pointers
/// including the normally excluded mouse
class MouseEnabledScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => PointerDeviceKind.values.toSet();
}

/// PageView for swiping through a list of images
class EasyImageViewPager extends StatefulWidget {
  final EasyImageProvider easyImageProvider;
  final PageController pageController;
  final bool doubleTapZoomable;

  /// Callback for when the scale has changed, only invoked at the end of
  /// an interaction.
  final void Function(double)? onScaleChanged;

  /// Create new instance, using the [easyImageProvider] to populate the [PageView],
  /// and the [pageController] to control the initial image index to display.
  /// The optional [doubleTapZoomable] boolean defaults to false and allows double tap to zoom.
  const EasyImageViewPager(
      {super.key,
      required this.easyImageProvider,
      required this.pageController,
      this.doubleTapZoomable = false,
      this.onScaleChanged});

  @override
  State<EasyImageViewPager> createState() => _EasyImageViewPagerState();
}

class _EasyImageViewPagerState extends State<EasyImageViewPager> {
  bool _pagingEnabled = true;
  bool _showAlt = false;

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: _pagingEnabled
          ? const PageScrollPhysics()
          : const NeverScrollableScrollPhysics(),
      key: GlobalObjectKey(widget.easyImageProvider),
      itemCount: widget.easyImageProvider.imageCount,
      controller: widget.pageController,
      scrollBehavior: MouseEnabledScrollBehavior(),
      itemBuilder: (context, index) {
        return Stack(
          children: [
            EasyImageView.imageWidget(
              widget.easyImageProvider.imageWidgetBuilder(context, index),
              key: Key('easy_image_view_$index'),
              doubleTapZoomable: widget.doubleTapZoomable,
              onScaleChanged: (scale) {
                if (widget.onScaleChanged != null) {
                  widget.onScaleChanged!(scale);
                }

                setState(() {
                  _pagingEnabled = scale <= 1.0;
                });
              },
            ),
            if (widget.easyImageProvider.getDescription(context, index) != null)
              Positioned(
                bottom: 10,
                child: _showAlt
                    ? SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          child: InkWell(
                            onTap: () => setState(() {
                              _showAlt = false;
                            }),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                widget.easyImageProvider
                                    .getDescription(context, index)!,
                              ),
                            ),
                          ),
                        ),
                      )
                    : IconButton(
                        onPressed: () => setState(() {
                              _showAlt = true;
                            }),
                        icon: const Icon(Icons.messenger_outlined)),
              ),
          ],
        );
      },
    );
  }
}
