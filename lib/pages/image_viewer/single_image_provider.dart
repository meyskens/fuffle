import 'package:flutter/material.dart';

import 'easy_image_provider.dart';

/// Convenience provider for a single image
class SingleImageProvider extends EasyImageProvider {
  final ImageProvider imageProvider;
  final String? imageDescription;

  SingleImageProvider(this.imageProvider, this.imageDescription);

  @override
  ImageProvider imageBuilder(BuildContext context, int index) {
    if (index != 0) {
      throw ArgumentError.value(
          initialIndex, 'index', 'The index value must only be 0.');
    }

    return imageProvider;
  }

  @override
  String? getDescription(BuildContext context, int index) {
    if (index != 0) {
      throw ArgumentError.value(
          initialIndex, 'index', 'The index value must only be 0.');
    }

    return imageDescription;
  }

  @override
  int get imageCount => 1;

  @override
  int get initialIndex => 0;
}
