import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_list_view/flutter_list_view.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'package:fuffle/widgets/core/account_switcher.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import 'package:fuffle/widgets/status/status_widget.dart';
import 'mentions_controller.dart';

class MentionsPageView extends StatelessWidget {
  final MentionsPageController controller;

  const MentionsPageView(this.controller, {super.key});
  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      currentIndex: 1,
      appBar: AppBar(
        leading: AccountSwitcher(
          account: controller.widget.account,
        ),
        title: const Text("Mentions"),
        actions: const [],
      ),
      body: RefreshIndicator(
        onRefresh: controller.refresh,
        child: Stack(
          children: [
            ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(dragDevices: {
                PointerDeviceKind.touch,
                PointerDeviceKind.mouse,
              }),
              child: FlutterListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                controller: controller.timelineController.listViewController,
                delegate: FlutterListViewDelegate(
                  (BuildContext context, int i) => VisibilityDetector(
                    key: ValueKey(
                        controller.timelineController.uiTimeline[i].id),
                    onVisibilityChanged: (visibilityInfo) {
                      if (visibilityInfo.visibleFraction > 0.5) {
                        controller.timelineController.markRead(i);
                      }
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      key: ValueKey(
                          controller.timelineController.uiTimeline[i].id),
                      children: [
                        StatusWidget(
                          ownAccount: controller.widget.account,
                          key: ValueKey(
                              controller.timelineController.uiTimeline[i].id),
                          status: controller.timelineController.uiTimeline[i],
                          onUpdate: controller.onUpdateStatus,
                        ),
                      ],
                    ),
                  ),
                  childCount: controller.timelineController.uiTimeline.length,
                  keepPosition: true,
                  keepPositionOffset: 0,
                  onItemKey: (i) =>
                      controller.timelineController.uiTimeline[i].id,
                  initIndex: controller.initIndex,
                  firstItemAlign: FirstItemAlign.end,
                  onIsPermanent: (_) => true,
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: InkWell(
                onDoubleTap: controller.timelineController.scrollToTop,
                child: Chip(
                  label: Text(
                    controller.timelineController.unreadCount.toString(),
                  ),
                  padding: EdgeInsets.zero,
                  backgroundColor: Theme.of(context).secondaryHeaderColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: const BorderSide(color: Colors.transparent)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
