import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_list_view/flutter_list_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:fuffle/pages/status_controller.dart';
import 'package:fuffle/utils/reply.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import 'package:fuffle/widgets/status/status_widget.dart';

class StatusPageView extends StatelessWidget {
  final StatusPageController controller;
  const StatusPageView(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    final children = [
      if (controller.statusContext != null)
        ListView.builder(
          shrinkWrap: true,
          physics:
              NeverScrollableScrollPhysics(parent: controller.scrollPhysics),
          itemCount: controller.statusContext!.ancestors.length,
          itemBuilder: (context, i) => StatusWidget(
            ownAccount: controller.widget.account,
            status: controller.statusContext!.ancestors[i],
            onUpdate: controller.onUpdateStatus,
            replyMode: true,
          ),
        ),
      if (controller.status != null) ...[
        StatusWidget(
          ownAccount: controller.widget.account,
          status: controller.status!,
          onUpdate: controller.onUpdateStatus,
          replyMode: true,
        ),
        const Divider(height: 1),
      ],
      if (controller.statusContext != null) ...[
        ListView.builder(
          shrinkWrap: true,
          physics:
              NeverScrollableScrollPhysics(parent: controller.scrollPhysics),
          itemCount: controller.statusContext!.descendants.length,
          itemBuilder: (context, i) => StatusWidget(
            ownAccount: controller.widget.account,
            status: controller.statusContext!.descendants[i],
            onUpdate: controller.onUpdateStatus,
            replyMode: true,
          ),
        ),
      ],
    ];
    return NavScaffold(
      appBar: AppBar(title: Text(L10n.of(context)!.viewPost)),
      floatingActionButton: FloatingActionButton(
        onPressed: () => openReplyCompose(
            context, controller.widget.account, controller.status!),
        child: const Icon(Icons.alternate_email),
      ),
      body: SmartRefresher(
        controller: controller.refreshController,
        enablePullDown: true,
        onRefresh: controller.refresh,
        child: FlutterListView(
          physics: controller.scrollPhysics,
          delegate: FlutterListViewDelegate(
            (context, i) => children[i],
            childCount: children.length,
            initIndex: controller.statusContext?.ancestors.length ?? 0,
          ),
        ),
      ),
    );
  }
}
