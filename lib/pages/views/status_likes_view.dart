import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:fuffle/model/mastodon/account.dart';
import 'package:fuffle/widgets/account_list_tile.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import '../status_likes.dart';

class StatusLikesPageView extends StatelessWidget {
  final StatusLikesPageController controller;
  const StatusLikesPageView(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      appBar: AppBar(title: Text(L10n.of(context)!.wasLikedBy)),
      body: FutureBuilder<List<Account>>(
        future: controller.request,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                L10n.of(context)!.oopsSomethingWentWrong,
                textAlign: TextAlign.center,
              ),
            );
          }
          final accounts = snapshot.data;
          if (accounts == null) {
            return const Center(child: CircularProgressIndicator());
          }
          if (accounts.isEmpty) {
            return Center(
              child: Text(
                L10n.of(context)!.suchEmpty,
                textAlign: TextAlign.center,
              ),
            );
          }
          return ListView.builder(
            itemCount: accounts.length,
            itemBuilder: (context, i) => AccountListTile(account: accounts[i]),
          );
        },
      ),
    );
  }
}
