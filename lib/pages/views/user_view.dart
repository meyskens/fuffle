import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/widgets/accounts/avatar.dart';
import 'package:fuffle/widgets/content/html.dart';
import 'package:fuffle/widgets/content/text.dart';
import 'package:fuffle/widgets/core/navbar.dart';
import 'package:fuffle/widgets/status/status_widget.dart';
import '../../utils/int_short_string_extension.dart';
import '../../widgets/accounts/relationship_pill.dart';
import '../user.dart';

enum PopupActions { block, mute, website, share }

class UserPageView extends StatelessWidget {
  final UserPageController controller;
  const UserPageView(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      appBar: AppBar(
        title: Text(L10n.of(context)!.profile),
        actions: [
          PopupMenuButton<PopupActions>(
            onSelected: controller.onPopupAction,
            child: const Icon(Icons.more_vert),
            itemBuilder: (context) => [
              PopupMenuItem(
                value: PopupActions.website,
                child: Row(
                  children: [
                    const Icon(Icons.person),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.externalProfile),
                  ],
                ),
              ),
              PopupMenuItem(
                value: PopupActions.share,
                child: Row(
                  children: [
                    const Icon(Icons.share),
                    const SizedBox(width: 12),
                    Text(L10n.of(context)!.shareLink),
                  ],
                ),
              ),
              if (controller.relationships != null)
                PopupMenuItem(
                  value: PopupActions.mute,
                  child: Row(
                    children: [
                      const Icon(Icons.volume_off),
                      const SizedBox(width: 12),
                      Text((controller.relationships?.muting ?? false)
                          ? L10n.of(context)!.unmuteUser
                          : L10n.of(context)!.muteUser),
                    ],
                  ),
                ),
              if (controller.relationships != null)
                PopupMenuItem(
                  value: PopupActions.block,
                  child: Row(
                    children: [
                      Icon((controller.relationships?.blocking ?? false)
                          ? Icons.shield
                          : Icons.shield_outlined),
                      const SizedBox(width: 12),
                      Text((controller.relationships?.blocking ?? false)
                          ? L10n.of(context)!.unblockUser
                          : L10n.of(context)!.blockUser),
                    ],
                  ),
                ),
            ],
          ),
        ],
      ),
      body: SmartRefresher(
        controller: controller.refreshController,
        enablePullDown: true,
        enablePullUp: controller.timeline?.isNotEmpty ?? false,
        onRefresh: controller.refresh,
        onLoading: controller.loadMore,
        child: ListView(
          physics: controller.scrollPhysics,
          controller: controller.scrollController,
          children: [
            if (controller.account != null) ...[
              if (!controller.account!.header.endsWith('missing.png'))
                ConstrainedBox(
                  constraints: const BoxConstraints(maxHeight: 256),
                  child: CachedNetworkImage(
                    imageUrl: Fuffle.of(context).allowAnimatedImages
                        ? controller.account!.header
                        : controller.account!.headerStatic,
                    fit: BoxFit.cover,
                    progressIndicatorBuilder: (_, __, ___) => const SizedBox(
                      height: 128,
                      child: BlurHash(
                        hash: AppConfigs.fallbackBlurHash,
                      ),
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Spacer(),
                    Material(
                      borderRadius: BorderRadius.circular(48),
                      elevation: 5,
                      child: Avatar(
                        account: controller.account!,
                        radius: 48,
                      ),
                    ),
                    const Divider(height: 1, thickness: 1),
                    const SizedBox(width: 16),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            FuffleText(
                              controller.account!.displayName.isNotEmpty
                                  ? controller.account!.displayName
                                  : controller.account!.username,
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                              emojis: controller.account!.emojis,
                            ),
                            if (controller.relationships!.followedBy! &&
                                controller.relationships!.following!)
                              const RelationPill(
                                label: 'Mutual',
                              )
                            else if (controller.relationships!.followedBy!)
                              const RelationPill(
                                label: 'Follows you',
                              ),
                          ],
                        ),
                        Text('@${controller.account?.acct}'),
                        const SizedBox(height: 16),
                        if (controller.isOwnUser)
                          OutlinedButton.icon(
                            icon: const Icon(Icons.settings),
                            label: Text(L10n.of(context)!.settings),
                            onPressed: controller.goToSettings,
                          ),
                        if (!controller.isOwnUser)
                          controller.relationships == null ||
                                  controller.loadFollowChanges
                              ? const Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 42.0),
                                  child: CircularProgressIndicator(),
                                )
                              : (controller.relationships!.following ?? false)
                                  ? OutlinedButton.icon(
                                      onPressed: controller.unfollow,
                                      icon: const Icon(
                                          Icons.check_circle_outline),
                                      label: Text(L10n.of(context)!.following),
                                    )
                                  : ElevatedButton.icon(
                                      onPressed: controller.follow,
                                      icon: const Icon(Icons.person_add),
                                      label: Text(L10n.of(context)!.follow),
                                    ),
                      ],
                    ),
                    const Spacer(),
                  ],
                ),
              ),
              if (controller.account?.note.isNotEmpty ?? false)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: FuffleHTML(
                    data: controller.account!.note,
                    emojis: controller.account!.emojis,
                  ),
                ),
              // account fields table
              if (controller.account?.fields != null) ...{
                Padding(
                    padding: const EdgeInsets.only(
                        bottom: 8.0, top: 4.0, left: 20.0, right: 20.0),
                    child: DataTable(
                      headingRowHeight: 0,
                      columnSpacing: 0,
                      columns: [
                        DataColumn(label: Container()),
                        DataColumn(label: Container()),
                      ],
                      rows: controller.account!.fields
                          .map((e) => DataRow(cells: [
                                DataCell(Text(
                                  e.name,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                )),
                                DataCell(SizedBox(
                                  width: 400, // TODO: fine tune this
                                  child: FuffleHTML(
                                      data: e.value,
                                      emojis: controller.account!.emojis),
                                )),
                              ]))
                          .toList(),
                    )),
              },
              const Divider(height: 1),
              // counter section
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _CountBox(
                      count: controller.account!.statusesCount ?? 0,
                      title: L10n.of(context)!.statuses,
                    ),
                    _CountBox(
                      count: controller.account!.followersCount ?? 0,
                      title: L10n.of(context)!.followers,
                    ),
                    _CountBox(
                      count: controller.account!.followingCount ?? 0,
                      title: L10n.of(context)!.following,
                    ),
                  ],
                ),
              ),
              const Divider(height: 1),
              // profile posts
              if (controller.timeline != null) ...{
                if (controller.timeline!.isEmpty)
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(L10n.of(context)!.suchEmpty),
                    ),
                  ),
                if (controller.timeline!.isNotEmpty)
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(
                        parent: controller.scrollPhysics),
                    controller: controller.scrollController,
                    itemCount: controller.timeline!.length,
                    itemBuilder: (context, i) => StatusWidget(
                      ownAccount: controller.widget.account,
                      status: controller.timeline![i],
                      onUpdate: controller.onUpdateStatus,
                    ),
                  ),
              },
              if (controller.timeline == null)
                const Center(
                    child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: CircularProgressIndicator(),
                )),
            ],
          ],
        ),
      ),
      currentIndex: controller.isOwnUser ? 4 : null,
      scrollController: controller.scrollController,
    );
  }
}

class _CountBox extends StatelessWidget {
  final String title;
  final int count;

  const _CountBox({
    required this.title,
    required this.count,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8),
      color: Theme.of(context).colorScheme.background,
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                count.shortString,
                style: const TextStyle(
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                title,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
