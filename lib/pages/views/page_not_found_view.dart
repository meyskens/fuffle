import 'package:flutter/material.dart';

import 'package:fuffle/widgets/core/navbar.dart';

class PageNotFoundRouteView extends StatelessWidget {
  const PageNotFoundRouteView({super.key});

  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      appBar: AppBar(),
      body: const Center(
        child: Text('Page not found...'),
      ),
    );
  }
}
