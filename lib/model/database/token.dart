import 'package:hive/hive.dart';

part 'token.g.dart';

const String _tokenBox = "fuffle_tokens";

@HiveType(typeId: 3)
class CreateApplicationResponse {
  @HiveField(0)
  String? instanceURL;

  @HiveField(1)
  final String id;
  @HiveField(2)
  final String name;
  @HiveField(3)
  final String? website;
  @HiveField(4)
  final String redirectUri;
  @HiveField(5)
  final String clientId;
  @HiveField(6)
  final String clientSecret;
  @HiveField(7)
  final String? vapidKey;

  CreateApplicationResponse({
    required this.id,
    required this.name,
    this.website,
    required this.redirectUri,
    required this.clientId,
    required this.clientSecret,
    required this.vapidKey,
  });

  CreateApplicationResponse.fromJson(Map<String, dynamic> json)
      : id = json['id'].toString(),
        name = json['name'],
        website = json['website'],
        redirectUri = json['redirect_uri'],
        clientId = json['client_id'].toString(),
        clientSecret = json['client_secret'],
        vapidKey = json['vapid_key']?.toString();

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'website': website,
        'redirect_uri': redirectUri,
        'client_id': clientId,
        'client_secret': clientSecret,
        'vapid_key': vapidKey,
      };

  static Future<CreateApplicationResponse?> getForInstance(
      String instanceURL) async {
    Box<CreateApplicationResponse> box = await Hive.openBox(_tokenBox);

    final resp = box.get(instanceURL);
    await box.close();

    return resp;
  }

  static Future<void> store(
      String instanceURL, CreateApplicationResponse res) async {
    res.instanceURL = instanceURL;

    Box<CreateApplicationResponse> box = await Hive.openBox(_tokenBox);
    await box.put(instanceURL, res);
    await box.close();
  }
}
