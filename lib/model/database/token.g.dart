// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CreateApplicationResponseAdapter
    extends TypeAdapter<CreateApplicationResponse> {
  @override
  final int typeId = 3;

  @override
  CreateApplicationResponse read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CreateApplicationResponse(
      id: fields[1] as String,
      name: fields[2] as String,
      website: fields[3] as String?,
      redirectUri: fields[4] as String,
      clientId: fields[5] as String,
      clientSecret: fields[6] as String,
      vapidKey: fields[7] as String?,
    )..instanceURL = fields[0] as String?;
  }

  @override
  void write(BinaryWriter writer, CreateApplicationResponse obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.instanceURL)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.website)
      ..writeByte(4)
      ..write(obj.redirectUri)
      ..writeByte(5)
      ..write(obj.clientId)
      ..writeByte(6)
      ..write(obj.clientSecret)
      ..writeByte(7)
      ..write(obj.vapidKey);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CreateApplicationResponseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
