// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FuffleAccountAdapter extends TypeAdapter<FuffleAccount> {
  @override
  final int typeId = 0;

  @override
  FuffleAccount read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FuffleAccount(
      accessTokenCredentials: fields[1] as AccessTokenCredentials,
      instanceURL: fields[0] as String,
      fuffleSettings: fields[2] as FuffleSettings?,
    )
      ..accountJSONCache = fields[3] as String?
      ..preferences = fields[4] as Preferences?;
  }

  @override
  void write(BinaryWriter writer, FuffleAccount obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.instanceURL)
      ..writeByte(1)
      ..write(obj.accessTokenCredentials)
      ..writeByte(2)
      ..write(obj.fuffleSettings)
      ..writeByte(3)
      ..write(obj.accountJSONCache)
      ..writeByte(4)
      ..write(obj.preferences);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FuffleAccountAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AccessTokenCredentialsAdapter
    extends TypeAdapter<AccessTokenCredentials> {
  @override
  final int typeId = 1;

  @override
  AccessTokenCredentials read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AccessTokenCredentials(
      accessToken: fields[0] as String?,
      tokenType: fields[1] as String?,
      scope: fields[2] as String?,
      createdAt: fields[3] as DateTime?,
    );
  }

  @override
  void write(BinaryWriter writer, AccessTokenCredentials obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.accessToken)
      ..writeByte(1)
      ..write(obj.tokenType)
      ..writeByte(2)
      ..write(obj.scope)
      ..writeByte(3)
      ..write(obj.createdAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccessTokenCredentialsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class FuffleSettingsAdapter extends TypeAdapter<FuffleSettings> {
  @override
  final int typeId = 2;

  @override
  FuffleSettings read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FuffleSettings(
      allowAnimatedImages: fields[0] as bool?,
      useInAppBrowser: fields[2] as bool?,
      unused: fields[3] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, FuffleSettings obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.allowAnimatedImages)
      ..writeByte(2)
      ..write(obj.useInAppBrowser)
      ..writeByte(3)
      ..write(obj.unused);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FuffleSettingsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
