import 'package:hive/hive.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/notification.dart';
import 'package:fuffle/model/database/mastodon/preferences.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/database/mastodon/status_visibility.dart';
import 'package:fuffle/model/database/token.dart';

void initAdapters() {
  Hive.registerAdapter(FuffleAccountAdapter());
  Hive.registerAdapter(AccessTokenCredentialsAdapter());
  Hive.registerAdapter(FuffleSettingsAdapter());
  Hive.registerAdapter(CreateApplicationResponseAdapter());
  Hive.registerAdapter(StatusAdapter());
  Hive.registerAdapter(StatusVisibilityAdapter());
  Hive.registerAdapter(MastodonNotificationAdapter());
  Hive.registerAdapter(NotificationTypeAdapter());
  Hive.registerAdapter(PreferencesAdapter());
}
