import 'dart:convert';

import 'package:hive/hive.dart';

import 'package:fuffle/model/database/mastodon/preferences.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'mastodon/status.dart';

part 'account.g.dart';

@HiveType(typeId: 0)
class FuffleAccount {
  @HiveField(0)
  String instanceURL;

  @HiveField(1)
  AccessTokenCredentials accessTokenCredentials;

  @HiveField(2)
  FuffleSettings? fuffleSettings;

  @HiveField(3)
  String? accountJSONCache; // TODO: improve this

  @HiveField(4)
  Preferences? preferences;

  @HiveField(5)
  String? instanceJSONCache;

  Account? mastodonAccount() => Account.fromJson(jsonDecode(accountJSONCache!));
  Instance instance() =>
      Instance.fromJson(jsonDecode(instanceJSONCache ?? "{}"));

  FuffleAccount({
    required this.accessTokenCredentials,
    required this.instanceURL,
    this.fuffleSettings,
  }) {
    fuffleSettings ??= FuffleSettings();
  }

  static Future<int> getCurrentAccountIndex() async {
    final box = await _openCurrentAccountDB();
    return box.get('current_account_index', defaultValue: 0)!;
  }

  static Future<void> setCurrentAccountIndex(int index) async {
    final box = await _openCurrentAccountDB();
    await box.put('current_account_index', index);
  }

  static Future<Box<FuffleAccount>> openDB() async {
    return Hive.openBox('fuffle_accounts');
  }

  static Future<Box<int>> _openCurrentAccountDB() async {
    const name = 'fuffle_current_account';
    late Box<int> box;
    if (!Hive.isBoxOpen(name)) {
      box = await Hive.openBox(name);
    } else {
      box = Hive.box(name);
    }
    return box;
  }

  Future<void> updateOwnAccount() async {
    final api = FuffleAPI(this);

    final mastodonAccount = await api.verifyAccountCredentials();
    final emojis = await api.getCustomEmojis();
    final instance = await api.getInstanceInfo();

    instanceJSONCache = jsonEncode(Instance(
            emojis: emojis,
            characterLimit: instance.configuration?.statuses?.maxCharacters)
        .toJson());
    preferences = await api.getAccountPreferences();
    accountJSONCache = jsonEncode(mastodonAccount.toJson());
  }
}

@HiveType(typeId: 1)
class AccessTokenCredentials {
  @HiveField(0)
  final String? accessToken;
  @HiveField(1)
  final String? tokenType;
  @HiveField(2)
  final String? scope;
  @HiveField(3)
  final DateTime? createdAt;

  AccessTokenCredentials({
    this.accessToken,
    this.tokenType,
    this.scope,
    this.createdAt,
  });

  AccessTokenCredentials.fromJson(Map<String, dynamic> json)
      : accessToken = json['access_token'],
        tokenType = json['token_type'],
        scope = json['scope'],
        createdAt = json['created_at'] == null
            ? null
            : DateTime.fromMillisecondsSinceEpoch(json['created_at']);

  Map<String, dynamic> toJson() => {
        'access_token': accessToken,
        'token_type': tokenType,
        if (scope != null) 'scope': scope,
        if (createdAt != null) 'created_at': createdAt!.millisecondsSinceEpoch,
      };
}

@HiveType(typeId: 2)
class FuffleSettings {
  @HiveField(0)
  bool? allowAnimatedImages;
  @HiveField(2)
  bool? useInAppBrowser;
  @HiveField(3)
  bool? unused;

  FuffleSettings({
    this.allowAnimatedImages = false,
    this.useInAppBrowser = true,
    this.unused = true,
  });
}

class Instance {
  int? characterLimit;
  List<Emoji>? emojis;

  Instance({this.characterLimit, this.emojis});

  Instance.fromJson(Map<String, dynamic> json) {
    characterLimit = json['characterLimit'];
    characterLimit ??= 500;

    if (json['emojis'] != null) {
      emojis = <Emoji>[];
      json['emojis'].forEach((v) {
        emojis!.add(Emoji.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['characterLimit'] = characterLimit;
    if (emojis != null) {
      data['emojis'] = emojis!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
