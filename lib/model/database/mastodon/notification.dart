import 'dart:convert';

import 'package:hive/hive.dart';

import 'package:fuffle/model/database/mastodon/status.dart';
import '../../mastodon/account.dart';

part 'notification.g.dart';

@HiveType(typeId: 7)
enum NotificationType {
  @HiveField(0)
  mention,
  @HiveField(1)
  favourite,
  @HiveField(2)
  follow,
  @HiveField(3)
  // ignore: constant_identifier_names
  follow_request,
  @HiveField(4)
  reblog,
  @HiveField(5)
  poll,
  @HiveField(6)
  status,
  @HiveField(7)
  update,
  @HiveField(8)
  unknown,
}

@HiveType(typeId: 6)
class MastodonNotification {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final NotificationType type;
  @HiveField(2)
  final DateTime createdAt;

  final Account? account;
  final Status? status;

  @HiveField(3)
  final String? jsonData;

  const MastodonNotification({
    required this.id,
    required this.type,
    required this.createdAt,
    this.account,
    this.status,
    this.jsonData,
  });

  factory MastodonNotification.fromJson(Map<String, dynamic> json) =>
      MastodonNotification(
        id: json['id'],
        type: NotificationType.values.firstWhere(
            (n) => n.toString().split('.').last == json['type'],
            orElse: () => NotificationType.unknown),
        createdAt: DateTime.parse(json['created_at']).toLocal(),
        account: Account.fromJson(json['account']),
        status: json['status'] != null ? Status.fromJson(json['status']) : null,
        jsonData: jsonEncode(json),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type.toString().split('.').last,
        'created_at': createdAt.toIso8601String(),
        'account': account?.toJson(),
        if (status != null) 'status': status!.toJson(),
      };

  MastodonNotification restoreFullDataFromJSON() =>
      MastodonNotification.fromJson(jsonDecode(jsonData!));
}
