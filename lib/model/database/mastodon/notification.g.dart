// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MastodonNotificationAdapter extends TypeAdapter<MastodonNotification> {
  @override
  final int typeId = 6;

  @override
  MastodonNotification read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MastodonNotification(
      id: fields[0] as String,
      type: fields[1] as NotificationType,
      createdAt: fields[2] as DateTime,
      jsonData: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, MastodonNotification obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.type)
      ..writeByte(2)
      ..write(obj.createdAt)
      ..writeByte(3)
      ..write(obj.jsonData);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MastodonNotificationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class NotificationTypeAdapter extends TypeAdapter<NotificationType> {
  @override
  final int typeId = 7;

  @override
  NotificationType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return NotificationType.mention;
      case 1:
        return NotificationType.favourite;
      case 2:
        return NotificationType.follow;
      case 3:
        return NotificationType.follow_request;
      case 4:
        return NotificationType.reblog;
      case 5:
        return NotificationType.poll;
      case 6:
        return NotificationType.status;
      case 7:
        return NotificationType.update;
      case 8:
        return NotificationType.unknown;
      default:
        return NotificationType.mention;
    }
  }

  @override
  void write(BinaryWriter writer, NotificationType obj) {
    switch (obj) {
      case NotificationType.mention:
        writer.writeByte(0);
        break;
      case NotificationType.favourite:
        writer.writeByte(1);
        break;
      case NotificationType.follow:
        writer.writeByte(2);
        break;
      case NotificationType.follow_request:
        writer.writeByte(3);
        break;
      case NotificationType.reblog:
        writer.writeByte(4);
        break;
      case NotificationType.poll:
        writer.writeByte(5);
        break;
      case NotificationType.status:
        writer.writeByte(6);
        break;
      case NotificationType.update:
        writer.writeByte(7);
        break;
      case NotificationType.unknown:
        writer.writeByte(8);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NotificationTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
