import 'dart:convert';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_langdetect/flutter_langdetect.dart' as langdetect;
import 'package:hive/hive.dart';

import 'package:fuffle/model/database/mastodon/status_visibility.dart';
import 'package:fuffle/model/mastodon/search_result.dart';
import '../../mastodon/account.dart';
import '../../mastodon/application.dart';
import '../../mastodon/card.dart';
import '../../mastodon/media_attachment.dart';

part 'status.g.dart';

@HiveType(typeId: 4)
class Status {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final DateTime createdAt;
  @HiveField(2)
  final String? inReplyToId;
  @HiveField(3)
  final String? inReplyToAccountId;
  @HiveField(4)
  final bool sensitive;
  @HiveField(5)
  final String spoilerText;
  @HiveField(6)
  final StatusVisibility visibility;
  @HiveField(7)
  final String? language;
  @HiveField(8)
  final String uri;
  @HiveField(9)
  final String? url;
  @HiveField(10)
  final int repliesCount;
  @HiveField(11)
  final int reblogsCount;
  @HiveField(12)
  final int favouritesCount;
  @HiveField(13)
  final bool? favourited;
  @HiveField(14)
  final bool? reblogged;
  @HiveField(15)
  final bool? muted;
  @HiveField(16)
  final bool? bookmarked;
  @HiveField(17)
  final String? content;
  @HiveField(18)
  final Status? reblog;

  // this has the full JSON data for the status
  // this may seem weird but it is used to store the full data
  // inside Hive without creating all types for binary representation
  // while we do not want to do any filtering or modification on the data.
  // It is horrible but do you have a better idea?
  @HiveField(19)
  final String? jsonData;

  // this is set internally when we extract the status from a notification
  @HiveField(20)
  final String? fromNotificationID;

  get needsContentWarning =>
      sensitive &&
      (spoilerText.isNotEmpty ||
          (mediaAttachments != null && mediaAttachments!.isNotEmpty));

  // These complex types are too cumbersome to store in Hive.
  // to get these yse the JSONData string instead
  final Application? application;
  final Account? account;
  final List<MediaAttachment>? mediaAttachments;
  final List<Mention>? mentions;
  final List<Hashtag>? tags;
  final List<Emoji>? emojis;
  final Card? card;
  final Map? poll;
  final String? fuffleLanguage;

  const Status({
    required this.id,
    required this.createdAt,
    this.inReplyToId,
    this.inReplyToAccountId,
    required this.sensitive,
    required this.spoilerText,
    required this.visibility,
    required this.language,
    required this.uri,
    required this.url,
    required this.repliesCount,
    required this.reblogsCount,
    required this.favouritesCount,
    this.favourited,
    this.reblogged,
    this.muted,
    this.bookmarked,
    this.content,
    required this.reblog,
    this.application,
    this.account,
    this.mediaAttachments,
    this.mentions,
    this.tags,
    this.emojis,
    this.card,
    this.poll,
    this.jsonData,
    this.fromNotificationID,
    this.fuffleLanguage,
  });

  Status withFromNotificationID(String id) {
    return Status(
      id: id,
      createdAt: createdAt,
      inReplyToId: inReplyToId,
      inReplyToAccountId: inReplyToAccountId,
      sensitive: sensitive,
      spoilerText: spoilerText,
      visibility: visibility,
      language: language,
      uri: uri,
      url: url,
      repliesCount: repliesCount,
      reblogsCount: reblogsCount,
      favouritesCount: favouritesCount,
      favourited: favourited,
      reblogged: reblogged,
      muted: muted,
      bookmarked: bookmarked,
      content: content,
      reblog: reblog,
      application: application,
      account: account,
      mediaAttachments: mediaAttachments,
      mentions: mentions,
      tags: tags,
      emojis: emojis,
      card: card,
      poll: poll,
      jsonData: jsonData,
      fuffleLanguage: fuffleLanguage,
      fromNotificationID: id,
    );
  }

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        id: json['id'],
        createdAt: DateTime.parse(json['created_at']).toLocal(),
        inReplyToId: json['in_reply_to_id'],
        inReplyToAccountId: json['in_reply_to_account_id'],
        sensitive: json['sensitive'],
        spoilerText: json['spoiler_text'],
        visibility: StatusVisibility.values.firstWhere(
            (v) => v.toString().split('.').last == json['visibility']),
        language: json['language'],
        uri: json['uri'],
        url: json['url'],
        repliesCount: json['replies_count'] ?? 0,
        reblogsCount: json['reblogs_count'] ?? 0,
        favouritesCount: json['favourites_count'] ?? 0,
        favourited: json['favourited'],
        reblogged: json['reblogged'],
        muted: json['muted'],
        bookmarked: json['bookmarked'],
        content: json['content'],
        reblog: json['reblog'] != null ? Status.fromJson(json['reblog']) : null,
        application: json['application'] == null
            ? null
            : Application.fromJson(json['application']),
        account: Account.fromJson(json['account']),
        mediaAttachments:
            List<Map<String, dynamic>>.from(json['media_attachments'])
                .map((json) => MediaAttachment.fromJson(json))
                .toList(),
        mentions: List<Map<String, dynamic>>.from(json['mentions'])
            .map((json) => Mention.fromJson(json))
            .toList(),
        tags: List<Map<String, dynamic>>.from(json['tags'])
            .map((json) => Hashtag.fromJson(json))
            .toList(),
        emojis: List<Map<String, dynamic>>.from(json['emojis'])
            .map((json) => Emoji.fromJson(json))
            .toList(),
        card: json['card'] == null ? null : Card.fromJson(json['card']),
        poll: json['poll'],
        fuffleLanguage: json['fuffleLanguage'] ?? detectLang(json['content']),
        jsonData: jsonEncode(json),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'created_at': createdAt.toIso8601String(),
        if (inReplyToId != null) 'in_reply_to_id': inReplyToId,
        if (inReplyToAccountId != null)
          'in_reply_to_account_id': inReplyToAccountId,
        'sensitive': sensitive,
        'spoiler_text': spoilerText,
        'visibility': visibility.toString().split('.').last,
        if (language != null) 'language': language,
        'uri': uri,
        if (url != null) 'url': url,
        'replies_count': repliesCount,
        'reblogs_count': reblogsCount,
        'favourites_count': favouritesCount,
        'favourited': favourited,
        'reblogged': reblogged,
        'muted': muted,
        'bookmarked': bookmarked,
        'content': content,
        if (reblog != null) 'reblog': reblog!.toJson(),
        if (application != null) 'application': application!.toJson(),
        'account': account?.toJson(),
        'media_attachments': mediaAttachments?.map((m) => m.toJson()).toList(),
        'mentions': mentions?.map((m) => m.toJson()).toList(),
        'tags': tags?.map((m) => m.toJson()).toList(),
        'emojis': emojis?.map((m) => m.toJson()).toList(),
        if (fuffleLanguage != null) 'fuffleLanguage': fuffleLanguage,
        if (card != null) 'card': card!.toJson(),
        if (poll != null) 'poll': poll,
      };

  Status restoreFullDataFromJSON() => Status.fromJson(jsonDecode(jsonData!));

  Future<void> preloadImages() async {
    List<MediaAttachment>? mediaAttachments = this.mediaAttachments;
    if (reblog?.mediaAttachments != null) {
      mediaAttachments = reblog!.mediaAttachments;
    }

    if (mediaAttachments != null) {
      for (var media in mediaAttachments) {
        if (media.type == MediaType.image) {
          DefaultCacheManager().getSingleFile(media.url.toString()).ignore();
        }
      }
    }

    List<Emoji>? emojis = this.emojis;
    if (reblog?.emojis != null) {
      emojis = reblog!.emojis;
    }

    if (emojis != null) {
      for (var emoji in emojis) {
        DefaultCacheManager().getSingleFile(emoji.url.toString()).ignore();
      }
    }
  }
}

class Mention {
  final String id;
  final String username;
  final String url;
  final String acct;

  const Mention({
    required this.id,
    required this.username,
    required this.url,
    required this.acct,
  });

  factory Mention.fromJson(Map<String, dynamic> json) => Mention(
        id: json['id'],
        username: json['username'],
        url: json['url'],
        acct: json['acct'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'username': username,
        'url': url,
        'acct': acct,
      };
}

class Emoji {
  final String shortcode;
  final String url;
  final String staticUrl;
  final bool visibleInPicker;
  final String? category;

  const Emoji({
    required this.shortcode,
    required this.url,
    required this.staticUrl,
    required this.visibleInPicker,
    this.category,
  });

  factory Emoji.fromJson(Map<String, dynamic> json) => Emoji(
        shortcode: json['shortcode'],
        url: json['url'],
        staticUrl: json['static_url'],
        visibleInPicker: json['visible_in_picker'],
        category: json['category'],
      );

  Map<String, dynamic> toJson() => {
        'shortcode': shortcode,
        'url': url,
        'static_url': staticUrl,
        'visible_in_picker': visibleInPicker,
        if (category != null) 'category': category,
      };
}

String detectLang(String? text) {
  if (text == null) return "";
  // TODO: add user configuration to trust the poster's language indication instead
  // the idea to use language heuristics here is because people often forget to set the language
  // detecting it will help people to determine the language of content warned posts with CWs in another language
  // or to generally provide information about the language of the post.
  // note: for project funding purposed this is AI, we are an AI driven corporation!
  try {
    // strip html tags
    text = text.replaceAll(RegExp(r'<[^>]*>'), '');
    // strip @username
    text = text.replaceAll(RegExp(r'@[a-zA-Z0-9_]*'), '');
    // strip meowing (this is the fediverse)
    text.replaceAll(
        RegExp(r'\b(m(e+o*w+|ia*u|e*w))\b', caseSensitive: false), '');
    // strip URLs
    text = text.replaceAll(RegExp(r'https?://\S+'), '');
    // strip emojis
    text = text.replaceAll(RegExp(r':\w+:'), '');
    // strip unicode emoji
    text = text.replaceAll(
        RegExp(
            r'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])'),
        '');
    if (text.length < 20) {
      return "";
    }
    return langdetect.detect(text);
  } catch (_) {
    return "";
  }
}
