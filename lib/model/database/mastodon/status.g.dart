// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class StatusAdapter extends TypeAdapter<Status> {
  @override
  final int typeId = 4;

  @override
  Status read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Status(
      id: fields[0] as String,
      createdAt: fields[1] as DateTime,
      inReplyToId: fields[2] as String?,
      inReplyToAccountId: fields[3] as String?,
      sensitive: fields[4] as bool,
      spoilerText: fields[5] as String,
      visibility: fields[6] as StatusVisibility,
      language: fields[7] as String?,
      uri: fields[8] as String,
      url: fields[9] as String?,
      repliesCount: fields[10] as int,
      reblogsCount: fields[11] as int,
      favouritesCount: fields[12] as int,
      favourited: fields[13] as bool?,
      reblogged: fields[14] as bool?,
      muted: fields[15] as bool?,
      bookmarked: fields[16] as bool?,
      content: fields[17] as String?,
      reblog: fields[18] as Status?,
      jsonData: fields[19] as String?,
      fromNotificationID: fields[20] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Status obj) {
    writer
      ..writeByte(21)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.createdAt)
      ..writeByte(2)
      ..write(obj.inReplyToId)
      ..writeByte(3)
      ..write(obj.inReplyToAccountId)
      ..writeByte(4)
      ..write(obj.sensitive)
      ..writeByte(5)
      ..write(obj.spoilerText)
      ..writeByte(6)
      ..write(obj.visibility)
      ..writeByte(7)
      ..write(obj.language)
      ..writeByte(8)
      ..write(obj.uri)
      ..writeByte(9)
      ..write(obj.url)
      ..writeByte(10)
      ..write(obj.repliesCount)
      ..writeByte(11)
      ..write(obj.reblogsCount)
      ..writeByte(12)
      ..write(obj.favouritesCount)
      ..writeByte(13)
      ..write(obj.favourited)
      ..writeByte(14)
      ..write(obj.reblogged)
      ..writeByte(15)
      ..write(obj.muted)
      ..writeByte(16)
      ..write(obj.bookmarked)
      ..writeByte(17)
      ..write(obj.content)
      ..writeByte(18)
      ..write(obj.reblog)
      ..writeByte(19)
      ..write(obj.jsonData)
      ..writeByte(20)
      ..write(obj.fromNotificationID);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StatusAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
