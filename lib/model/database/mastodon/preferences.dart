import 'package:hive/hive.dart';

import 'package:fuffle/model/database/mastodon/status_visibility.dart';

part 'preferences.g.dart';

@HiveType(typeId: 8)
class Preferences {
  @HiveField(0)
  StatusVisibility? postingDefaultVisibility;
  @HiveField(1)
  bool? postingDefaultSensitive;
  @HiveField(2)
  String? postingDefaultLanguage;
  @HiveField(3)
  String? readingExpandMedia;
  @HiveField(4)
  bool? readingExpandSpoilers;

  Preferences(
      {this.postingDefaultVisibility,
      this.postingDefaultSensitive,
      this.postingDefaultLanguage,
      this.readingExpandMedia,
      this.readingExpandSpoilers});

  Preferences.fromJson(Map<String, dynamic> json) {
    postingDefaultVisibility = StatusVisibility.values.firstWhere((v) =>
        v.toString().split('.').last == json['posting:default:visibility']);
    postingDefaultSensitive = json['posting:default:sensitive'];
    postingDefaultLanguage = json['posting:default:language'];
    readingExpandMedia = json['reading:expand:media'];
    readingExpandSpoilers = json['reading:expand:spoilers'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['posting:default:visibility'] =
        postingDefaultVisibility.toString().split('.').last;
    data['posting:default:sensitive'] = postingDefaultSensitive;
    data['posting:default:language'] = postingDefaultLanguage;
    data['reading:expand:media'] = readingExpandMedia;
    data['reading:expand:spoilers'] = readingExpandSpoilers;
    return data;
  }
}
