import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:hive/hive.dart';

part 'status_visibility.g.dart';

@HiveType(typeId: 5)
enum StatusVisibility {
  @HiveField(0)
  public,
  @HiveField(1)
  unlisted,
  @HiveField(2)
  private,
  @HiveField(3)
  direct,
}

extension StatusVisibilityLocalization on StatusVisibility {
  IconData get icon {
    switch (this) {
      case StatusVisibility.public:
        return Icons.public_outlined;
      case StatusVisibility.unlisted:
        return Icons.lock_open_rounded;
      case StatusVisibility.private:
        return Icons.lock_rounded;
      case StatusVisibility.direct:
        return Icons.mail_outline;
    }
  }

  String toLocalizedString(BuildContext context) {
    switch (this) {
      case StatusVisibility.public:
        return L10n.of(context)!.worldWide;
      case StatusVisibility.unlisted:
        return L10n.of(context)!.notListed;
      case StatusVisibility.private:
        return L10n.of(context)!.followersOnly;
      case StatusVisibility.direct:
        return L10n.of(context)!.mentionsOnly;
    }
  }
}
