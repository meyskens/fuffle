// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preferences.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PreferencesAdapter extends TypeAdapter<Preferences> {
  @override
  final int typeId = 8;

  @override
  Preferences read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Preferences(
      postingDefaultVisibility: fields[0] as StatusVisibility?,
      postingDefaultSensitive: fields[1] as bool?,
      postingDefaultLanguage: fields[2] as String?,
      readingExpandMedia: fields[3] as String?,
      readingExpandSpoilers: fields[4] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, Preferences obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.postingDefaultVisibility)
      ..writeByte(1)
      ..write(obj.postingDefaultSensitive)
      ..writeByte(2)
      ..write(obj.postingDefaultLanguage)
      ..writeByte(3)
      ..write(obj.readingExpandMedia)
      ..writeByte(4)
      ..write(obj.readingExpandSpoilers);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PreferencesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
