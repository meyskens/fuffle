// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_visibility.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class StatusVisibilityAdapter extends TypeAdapter<StatusVisibility> {
  @override
  final int typeId = 5;

  @override
  StatusVisibility read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return StatusVisibility.public;
      case 1:
        return StatusVisibility.unlisted;
      case 2:
        return StatusVisibility.private;
      case 3:
        return StatusVisibility.direct;
      default:
        return StatusVisibility.public;
    }
  }

  @override
  void write(BinaryWriter writer, StatusVisibility obj) {
    switch (obj) {
      case StatusVisibility.public:
        writer.writeByte(0);
        break;
      case StatusVisibility.unlisted:
        writer.writeByte(1);
        break;
      case StatusVisibility.private:
        writer.writeByte(2);
        break;
      case StatusVisibility.direct:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StatusVisibilityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
