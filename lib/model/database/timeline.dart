import 'package:flutter/material.dart';

import 'package:hive/hive.dart';
import 'package:mutex/mutex.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';

// PositionedTimelineWriteMutex makes sure that only one write operation is done at a time.
// this prevents both the UI refresh and background refresh from fetching data at the same time
// resulting in double posts. Is a singleton overkill for all actions? Maybe. But it prevents all cases.
// Double posts also result in malfunctioning of the list view controller.
class PositionedTimelineWriteMutex {
  static final PositionedTimelineWriteMutex _singleton =
      PositionedTimelineWriteMutex._internal();

  final _updateMutex = Mutex();

  factory PositionedTimelineWriteMutex() {
    return _singleton;
  }

  Future<void> acquire() async {
    await _updateMutex.acquire();
  }

  void release() {
    _updateMutex.release();
  }

  PositionedTimelineWriteMutex._internal();
}

// PositionedTimeline is the core of the Fuffle timelines.
// it is a generic class for use of the Status Timelines, mentions and notifications.
// The PositionedTimeline is a self maintaining dynamic list of items that returns them in chronological order.
// Internal the list is reversed in order to allow for feeding of additional date without changing the position.
// The PositionedTimeline is designed to:
// - store a cache of items on a screen
// - keep the current scroll position between sessions
// - give a count of unread items for completionist users
// - is designed to be used with the Hive database for persistence
// - to self garbage collect when needed
class PositionedTimeline<T> {
  late String _boxName;
  late Box<T> _box;
  int _readIndex = 0;
  late TimeLinePosition _storedIndex;
  int maxItems;
  bool _initDone = false;
  final updateMutex = PositionedTimelineWriteMutex();
  late Fuffle fuffle;

  PositionedTimeline(BuildContext context, FuffleAccount account, String name,
      {this.maxItems = 1000}) {
    _readIndex = 0;
    _boxName =
        '${makeURLNameSafe(account.instanceURL.toString())}-${account.mastodonAccount()!.id}-$name';
  }

  Future<void> init() async {
    if (_initDone) {
      return;
    }
    _box = await Hive.openBox<T>(_boxName);
    _storedIndex = TimeLinePosition(timeLineName: _boxName);
    await _storedIndex.init();
    _readIndex = _storedIndex.position;
    await garbageCollect();
    _initDone = true;
  }

  get timeline => _box.values.toList().reversed.toList();

  void markRead(int i) {
    final newRead = (_box.length - 1) - i;
    if (newRead > _readIndex) {
      _readIndex = newRead;
    }
    _storedIndex.position = _readIndex;
  }

  int get readIndex {
    final stored = _storedIndex.position;
    if (stored < 0) {
      // this means we have a new timeline
      return _readIndex;
    }
    return unreadCount;
  }

  int get unreadCount => _box.length - _readIndex - 1;

  Future<void> garbageCollect() async {
    if (_box.length > maxItems) {
      int toRemove = _box.length - maxItems;
      for (int i = 0; i < toRemove; i++) {
        await _box.deleteAt(0);
        if (_readIndex > 0) _readIndex--;
      }
    }
    if (_readIndex > _box.length) {
      _readIndex = _box.length - 1;
    }

    await _box.compact();
  }

  void add(T item) {
    _box.add(item);
  }

  void addList(List<T> items) {
    _box.addAll(items.reversed);
  }

  void deleteWhere(bool Function(T element) test) {
    final List<dynamic> toDelete = [];

    for (var key in _box.keys) {
      if (test(_box.get(key) as T)) {
        toDelete.add(key);
      }
    }

    for (var key in toDelete) {
      _box.delete(key);
    }
  }

  void replaceWhere(T newElement, bool Function(T element) test) {
    final List<dynamic> toReplace = [];

    for (var key in _box.keys) {
      if (test(_box.get(key) as T)) {
        toReplace.add(key);
      }
    }

    for (var key in toReplace) {
      _box.put(key, newElement);
    }
  }

  // reversed as the order in tbe box is reversed
  T get first => _box.values.last;

  // reversed as the order in tbe box is reversed
  T get last => _box.values.first;

  int get length => _box.length;
}

class TimeLinePosition {
  late String timeLineName;
  late Box<int> _box;
  bool _initDone = false;

  int _positionCache = -1; // -1 is returned if never was set

  TimeLinePosition({required this.timeLineName});

  Future<void> init() async {
    if (_initDone) {
      return;
    }
    _box = await Hive.openBox<int>("${timeLineName}_position");
    if (_box.isEmpty) {
      position = -1;
    }
    _positionCache = _box.getAt(0) ?? -1;
    _initDone = true;
  }

  int get position => _positionCache;
  set position(int position) {
    if (_box.isEmpty) {
      _box.add(position);
    }
    _box.putAt(0, position);
    _positionCache = position;
  }
}

String makeURLNameSafe(String name) {
  return name.toLowerCase().replaceAll(RegExp(r'[^a-z0-9]'), '-');
}
