class MastodonInstance {
  String? uri;
  String? title;
  String? shortDescription;
  String? description;
  String? email;
  String? version;
  Urls? urls;
  Stats? stats;
  String? thumbnail;
  List<String>? languages;
  bool? registrations;
  bool? approvalRequired;
  bool? invitesEnabled;
  Configuration? configuration;
  ContactAccount? contactAccount;
  List<Rules>? rules;

  MastodonInstance(
      {this.uri,
      this.title,
      this.shortDescription,
      this.description,
      this.email,
      this.version,
      this.urls,
      this.stats,
      this.thumbnail,
      this.languages,
      this.registrations,
      this.approvalRequired,
      this.invitesEnabled,
      this.configuration,
      this.contactAccount,
      this.rules});

  MastodonInstance.fromJson(Map<String, dynamic> json) {
    uri = json['uri'];
    title = json['title'];
    shortDescription = json['short_description'];
    description = json['description'];
    email = json['email'];
    version = json['version'];
    urls = json['urls'] != null ? Urls.fromJson(json['urls']) : null;
    stats = json['stats'] != null ? Stats.fromJson(json['stats']) : null;
    thumbnail = json['thumbnail'];
    languages = json['languages'].cast<String>();
    registrations = json['registrations'];
    approvalRequired = json['approval_required'];
    invitesEnabled = json['invites_enabled'];
    configuration = json['configuration'] != null
        ? Configuration.fromJson(json['configuration'])
        : null;
    contactAccount = json['contact_account'] != null
        ? ContactAccount.fromJson(json['contact_account'])
        : null;
    if (json['rules'] != null) {
      rules = <Rules>[];
      json['rules'].forEach((v) {
        rules!.add(Rules.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['uri'] = uri;
    data['title'] = title;
    data['short_description'] = shortDescription;
    data['description'] = description;
    data['email'] = email;
    data['version'] = version;
    if (urls != null) {
      data['urls'] = urls!.toJson();
    }
    if (stats != null) {
      data['stats'] = stats!.toJson();
    }
    data['thumbnail'] = thumbnail;
    data['languages'] = languages;
    data['registrations'] = registrations;
    data['approval_required'] = approvalRequired;
    data['invites_enabled'] = invitesEnabled;
    if (configuration != null) {
      data['configuration'] = configuration!.toJson();
    }
    if (contactAccount != null) {
      data['contact_account'] = contactAccount!.toJson();
    }
    if (rules != null) {
      data['rules'] = rules!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Urls {
  String? streamingApi;

  Urls({this.streamingApi});

  Urls.fromJson(Map<String, dynamic> json) {
    streamingApi = json['streaming_api'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['streaming_api'] = streamingApi;
    return data;
  }
}

class Stats {
  int? userCount;
  int? statusCount;
  int? domainCount;

  Stats({this.userCount, this.statusCount, this.domainCount});

  Stats.fromJson(Map<String, dynamic> json) {
    userCount = json['user_count'];
    statusCount = json['status_count'];
    domainCount = json['domain_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_count'] = userCount;
    data['status_count'] = statusCount;
    data['domain_count'] = domainCount;
    return data;
  }
}

class Configuration {
  Accounts? accounts;
  Statuses? statuses;
  MediaAttachments? mediaAttachments;
  Polls? polls;

  Configuration(
      {this.accounts, this.statuses, this.mediaAttachments, this.polls});

  Configuration.fromJson(Map<String, dynamic> json) {
    accounts =
        json['accounts'] != null ? Accounts.fromJson(json['accounts']) : null;
    statuses =
        json['statuses'] != null ? Statuses.fromJson(json['statuses']) : null;
    mediaAttachments = json['media_attachments'] != null
        ? MediaAttachments.fromJson(json['media_attachments'])
        : null;
    polls = json['polls'] != null ? Polls.fromJson(json['polls']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (accounts != null) {
      data['accounts'] = accounts!.toJson();
    }
    if (statuses != null) {
      data['statuses'] = statuses!.toJson();
    }
    if (mediaAttachments != null) {
      data['media_attachments'] = mediaAttachments!.toJson();
    }
    if (polls != null) {
      data['polls'] = polls!.toJson();
    }
    return data;
  }
}

class Accounts {
  int? maxFeaturedTags;

  Accounts({this.maxFeaturedTags});

  Accounts.fromJson(Map<String, dynamic> json) {
    maxFeaturedTags = json['max_featured_tags'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['max_featured_tags'] = maxFeaturedTags;
    return data;
  }
}

class Statuses {
  int? maxCharacters;
  int? maxMediaAttachments;
  int? charactersReservedPerUrl;

  Statuses(
      {this.maxCharacters,
      this.maxMediaAttachments,
      this.charactersReservedPerUrl});

  Statuses.fromJson(Map<String, dynamic> json) {
    maxCharacters = json['max_characters'];
    maxMediaAttachments = json['max_media_attachments'];
    charactersReservedPerUrl = json['characters_reserved_per_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['max_characters'] = maxCharacters;
    data['max_media_attachments'] = maxMediaAttachments;
    data['characters_reserved_per_url'] = charactersReservedPerUrl;
    return data;
  }
}

class MediaAttachments {
  List<String>? supportedMimeTypes;
  int? imageSizeLimit;
  int? imageMatrixLimit;
  int? videoSizeLimit;
  int? videoFrameRateLimit;
  int? videoMatrixLimit;

  MediaAttachments(
      {this.supportedMimeTypes,
      this.imageSizeLimit,
      this.imageMatrixLimit,
      this.videoSizeLimit,
      this.videoFrameRateLimit,
      this.videoMatrixLimit});

  MediaAttachments.fromJson(Map<String, dynamic> json) {
    supportedMimeTypes = json['supported_mime_types'].cast<String>();
    imageSizeLimit = json['image_size_limit'];
    imageMatrixLimit = json['image_matrix_limit'];
    videoSizeLimit = json['video_size_limit'];
    videoFrameRateLimit = json['video_frame_rate_limit'];
    videoMatrixLimit = json['video_matrix_limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['supported_mime_types'] = supportedMimeTypes;
    data['image_size_limit'] = imageSizeLimit;
    data['image_matrix_limit'] = imageMatrixLimit;
    data['video_size_limit'] = videoSizeLimit;
    data['video_frame_rate_limit'] = videoFrameRateLimit;
    data['video_matrix_limit'] = videoMatrixLimit;
    return data;
  }
}

class Polls {
  int? maxOptions;
  int? maxCharactersPerOption;
  int? minExpiration;
  int? maxExpiration;

  Polls(
      {this.maxOptions,
      this.maxCharactersPerOption,
      this.minExpiration,
      this.maxExpiration});

  Polls.fromJson(Map<String, dynamic> json) {
    maxOptions = json['max_options'];
    maxCharactersPerOption = json['max_characters_per_option'];
    minExpiration = json['min_expiration'];
    maxExpiration = json['max_expiration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['max_options'] = maxOptions;
    data['max_characters_per_option'] = maxCharactersPerOption;
    data['min_expiration'] = minExpiration;
    data['max_expiration'] = maxExpiration;
    return data;
  }
}

class ContactAccount {
  String? id;
  String? username;
  String? acct;
  String? displayName;
  bool? locked;
  bool? bot;
  bool? discoverable;
  bool? indexable;
  bool? group;
  String? createdAt;
  String? note;
  String? url;
  String? uri;
  String? avatar;
  String? avatarStatic;
  String? header;
  String? headerStatic;
  int? followersCount;
  int? followingCount;
  int? statusesCount;
  String? lastStatusAt;
  bool? hideCollections;
  bool? noindex;
  List<Emojis>? emojis;
  List<Roles>? roles;
  List<Fields>? fields;

  ContactAccount(
      {this.id,
      this.username,
      this.acct,
      this.displayName,
      this.locked,
      this.bot,
      this.discoverable,
      this.indexable,
      this.group,
      this.createdAt,
      this.note,
      this.url,
      this.uri,
      this.avatar,
      this.avatarStatic,
      this.header,
      this.headerStatic,
      this.followersCount,
      this.followingCount,
      this.statusesCount,
      this.lastStatusAt,
      this.hideCollections,
      this.noindex,
      this.emojis,
      this.roles,
      this.fields});

  ContactAccount.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    acct = json['acct'];
    displayName = json['display_name'];
    locked = json['locked'];
    bot = json['bot'];
    discoverable = json['discoverable'];
    indexable = json['indexable'];
    group = json['group'];
    createdAt = json['created_at'];
    note = json['note'];
    url = json['url'];
    uri = json['uri'];
    avatar = json['avatar'];
    avatarStatic = json['avatar_static'];
    header = json['header'];
    headerStatic = json['header_static'];
    followersCount = json['followers_count'];
    followingCount = json['following_count'];
    statusesCount = json['statuses_count'];
    lastStatusAt = json['last_status_at'];
    hideCollections = json['hide_collections'];
    noindex = json['noindex'];
    if (json['emojis'] != null) {
      emojis = <Emojis>[];
      json['emojis'].forEach((v) {
        emojis!.add(Emojis.fromJson(v));
      });
    }
    if (json['roles'] != null) {
      roles = <Roles>[];
      json['roles'].forEach((v) {
        roles!.add(Roles.fromJson(v));
      });
    }
    if (json['fields'] != null) {
      fields = <Fields>[];
      json['fields'].forEach((v) {
        fields!.add(Fields.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['acct'] = acct;
    data['display_name'] = displayName;
    data['locked'] = locked;
    data['bot'] = bot;
    data['discoverable'] = discoverable;
    data['indexable'] = indexable;
    data['group'] = group;
    data['created_at'] = createdAt;
    data['note'] = note;
    data['url'] = url;
    data['uri'] = uri;
    data['avatar'] = avatar;
    data['avatar_static'] = avatarStatic;
    data['header'] = header;
    data['header_static'] = headerStatic;
    data['followers_count'] = followersCount;
    data['following_count'] = followingCount;
    data['statuses_count'] = statusesCount;
    data['last_status_at'] = lastStatusAt;
    data['hide_collections'] = hideCollections;
    data['noindex'] = noindex;
    if (emojis != null) {
      data['emojis'] = emojis!.map((v) => v.toJson()).toList();
    }
    if (roles != null) {
      data['roles'] = roles!.map((v) => v.toJson()).toList();
    }
    if (fields != null) {
      data['fields'] = fields!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Emojis {
  String? shortcode;
  String? url;
  String? staticUrl;
  bool? visibleInPicker;

  Emojis({this.shortcode, this.url, this.staticUrl, this.visibleInPicker});

  Emojis.fromJson(Map<String, dynamic> json) {
    shortcode = json['shortcode'];
    url = json['url'];
    staticUrl = json['static_url'];
    visibleInPicker = json['visible_in_picker'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['shortcode'] = shortcode;
    data['url'] = url;
    data['static_url'] = staticUrl;
    data['visible_in_picker'] = visibleInPicker;
    return data;
  }
}

class Roles {
  String? id;
  String? name;
  String? color;

  Roles({this.id, this.name, this.color});

  Roles.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['color'] = color;
    return data;
  }
}

class Fields {
  String? name;
  String? value;
  String? verifiedAt;

  Fields({this.name, this.value, this.verifiedAt});

  Fields.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
    verifiedAt = json['verified_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['value'] = value;
    data['verified_at'] = verifiedAt;
    return data;
  }
}

class Rules {
  String? id;
  String? text;

  Rules({this.id, this.text});

  Rules.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['text'] = text;
    return data;
  }
}
