import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:hive/hive.dart';
import 'package:provider/provider.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/token.dart';
import 'package:fuffle/model/fuffle/login.dart';

class Fuffle {
  late Box<FuffleAccount> accountDB;
  int currentAccountIndex =
      -1; // should only be used initially, the router should indicate the account
  bool isInBackground = false;

  bool get isLoggedIn => currentAccountIndex > -1;

  Fuffle();

  Future<void> init() async {
    accountDB = await FuffleAccount.openDB();
    await switchToAccountAt(await FuffleAccount.getCurrentAccountIndex());
  }

  factory Fuffle.of(BuildContext context) =>
      Provider.of<Fuffle>(context, listen: false);

  Widget builder(BuildContext context, Widget? child) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          systemNavigationBarColor: Theme.of(context).colorScheme.background,
          systemNavigationBarIconBrightness:
              Theme.of(context).brightness == Brightness.light
                  ? Brightness.dark
                  : Brightness.light,
        ),
      );
    });
    return Provider(create: (_) => this, child: child ?? Container());
  }

  Future<void> switchToAccountAt(int index) async {
    final account = accountDB.get(index);
    if (account == null) {
      return;
    }
    currentAccountIndex = index;

    await account.updateOwnAccount();
    await saveAccount(index, account);

    await FuffleAccount.setCurrentAccountIndex(currentAccountIndex);
  }

  Future<void> saveAccount(int index, FuffleAccount account) =>
      accountDB.put(index, account);

  FuffleAccount getAccountAt(int index) => accountDB.get(index)!;

  bool get allowAnimatedImages =>
      getAccountAt(currentAccountIndex).fuffleSettings?.allowAnimatedImages ??
      true;
  set allowAnimatedImages(bool b) {
    final acc = getAccountAt(currentAccountIndex);
    acc.fuffleSettings!.allowAnimatedImages = b;

    saveAccount(currentAccountIndex, acc);
  }

  bool get useInAppBrowser =>
      getAccountAt(currentAccountIndex).fuffleSettings?.useInAppBrowser ?? true;
  set useInAppBrowser(bool b) {
    final acc = getAccountAt(currentAccountIndex);
    acc.fuffleSettings!.useInAppBrowser = b;

    saveAccount(currentAccountIndex, acc);
  }

  Future<void> logout({bool revoke = true}) async {
    try {
      final createApplicationResponse =
          await CreateApplicationResponse.getForInstance(
              getAccountAt(currentAccountIndex).instanceURL);
      if (createApplicationResponse == null) return;
      if (revoke) {
        await FuffleLogin().revokeToken(
          createApplicationResponse.clientId,
          createApplicationResponse.clientSecret,
          getAccountAt(currentAccountIndex).accessTokenCredentials.accessToken!,
          getAccountAt(currentAccountIndex).instanceURL,
        );
      }
    } finally {
      try {
        await accountDB.delete(currentAccountIndex);
        await _reArrangeAccountIndexes();
      } catch (_) {}
    }
  }

  Future<void> _reArrangeAccountIndexes() async {
    final accounts = accountDB.values.toList();
    await accountDB.clear();
    for (int i = 0; i < accounts.length; i++) {
      await accountDB.put(i, accounts[i]);
    }

    if (accounts.isNotEmpty) {
      currentAccountIndex = 0;
    } else {
      currentAccountIndex = -1;
    }
  }
}
