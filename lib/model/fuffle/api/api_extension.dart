import 'package:fuffle/model/database/mastodon/notification.dart';
import 'package:fuffle/model/database/mastodon/preferences.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/database/mastodon/status_visibility.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'package:fuffle/model/mastodon/chunk.dart';
import 'package:fuffle/model/mastodon/conversation.dart';
import 'package:fuffle/model/mastodon/instance.dart';
import 'package:fuffle/model/mastodon/media_attachment.dart';
import 'package:fuffle/model/mastodon/push_subscription.dart';
import 'package:fuffle/model/mastodon/read_markers.dart';
import 'package:fuffle/model/mastodon/relationships.dart';
import 'package:fuffle/model/mastodon/search_result.dart';
import 'package:fuffle/model/mastodon/status_context.dart';
import 'api.dart';

extension FuffleApiExtension on FuffleAPI {
  Future<Account> verifyAccountCredentials() => request(
        RequestType.get,
        '/api/v1/accounts/verify_credentials',
      ).then((json) => Account.fromJson(json));

  Future<List<Status>> requestHomeTimeline({
    String? maxId,
    String? sinceId,
    String? limit,
  }) =>
      request(RequestType.get, '/api/v1/timelines/home', query: {
        if (maxId != null) 'max_id': maxId,
        if (sinceId != null) 'min_id': sinceId,
        if (limit != null) 'limit': limit,
      }).then(
        (json) =>
            (json['chunk'] as List).map((j) => Status.fromJson(j)).toList(),
      );

  Future<List<Status>> requestPublicTimeline({
    String? maxId,
    bool mediaOnly = true,
    bool local = false,
  }) =>
      request(RequestType.get, '/api/v1/timelines/public', query: {
        if (maxId != null) 'max_id': maxId,
        'only_media': mediaOnly.toString(),
        'limit': '30',
        'local': local.toString(),
      }).then(
        (json) =>
            (json['chunk'] as List).map((j) => Status.fromJson(j)).toList(),
      );

  Future<List<Status>> requestTagTimeline(String tag, {String? maxId}) =>
      request(
          RequestType.get, '/api/v1/timelines/tag/${Uri.encodeComponent(tag)}',
          query: {
            if (maxId != null) 'max_id': maxId,
          }).then(
        (json) =>
            (json['chunk'] as List).map((j) => Status.fromJson(j)).toList(),
      );

  Future<List<Status>> requestUserTimeline(
    String userId, {
    String? maxId,
    bool excludeReplies = true,
    bool onlyMedia = false,
  }) =>
      request(RequestType.get,
          '/api/v1/accounts/${Uri.encodeComponent(userId)}/statuses',
          query: {
            if (maxId != null) 'max_id': maxId,
            'exclude_replies': excludeReplies.toString(),
            'only_media': onlyMedia.toString(),
            'limit': '21',
          }).then(
        (json) =>
            (json['chunk'] as List).map((j) => Status.fromJson(j)).toList(),
      );

  Future<Chunk<MastodonNotification>> getNotifications({
    String? maxId,
    String? sinceId,
    String? limit,
    List<NotificationType>? filter,
  }) =>
      request(RequestType.get, '/api/v1/notifications', query: {
        if (maxId != null) 'max_id': maxId,
        if (sinceId != null) 'min_id': sinceId,
        if (limit != null) 'limit': limit,
        if (filter != null)
          'types[]': filter.map((f) => f.toString().split(".").last).toList(),
      }).then(
        (json) => Chunk.fromJson(json, (m) => MastodonNotification.fromJson(m)),
      );

  Future<List<Conversation>> requestConversations({String? maxId}) =>
      request(RequestType.get, '/api/v1/conversations', query: {
        if (maxId != null) 'max_id': maxId,
      }).then(
        (json) => (json['chunk'] as List)
            .map((j) => Conversation.fromJson(j))
            .toList(),
      );

  Future<Chunk<Account>> requestFollowers(String id, {String? maxId}) =>
      request(RequestType.get,
          '/api/v1/accounts/${Uri.encodeComponent(id)}/followers',
          query: {
            if (maxId != null) 'max_id': maxId,
          }).then(
        (json) => Chunk.fromJson(json, (m) => Account.fromJson(m)),
      );

  Future<Chunk<Account>> requestFollowing(String id, {String? maxId}) =>
      request(RequestType.get,
          '/api/v1/accounts/${Uri.encodeComponent(id)}/following',
          query: {
            if (maxId != null) 'max_id': maxId,
          }).then(
        (json) => Chunk.fromJson(json, (m) => Account.fromJson(m)),
      );

  Future<Status> getStatus(String id) => request(
        RequestType.get,
        '/api/v1/statuses/${Uri.encodeComponent(id)}',
      ).then(
        (json) => Status.fromJson(json),
      );

  Future<StatusContext> getStatusContext(String id) => request(
        RequestType.get,
        '/api/v1/statuses/${Uri.encodeComponent(id)}/context',
      ).then(
        (json) => StatusContext.fromJson(json),
      );

  Future<Account> loadAccount(String username) => request(
        RequestType.get,
        '/api/v1/accounts/${Uri.encodeComponent(username)}',
      ).then(
        (json) => Account.fromJson(json),
      );

  Future<Relationships> getRelationship(String username) => request(
        RequestType.get,
        '/api/v1/accounts/relationships',
        query: {'id': username},
      ).then(
        (json) => Relationships.fromJson(json['chunk'].single),
      );

  Future<void> report(
    String accountId,
    List<String> statusIds,
    String comment,
  ) =>
      request(RequestType.post, '/api/v1/report', data: {
        'account_id': accountId,
        'status_ids': statusIds,
        'comment': comment,
        'forward': true,
      });

  Future<SearchResult> search(String query, {String? maxId}) =>
      request(RequestType.get, '/api/v2/search', query: {
        'q': query,
        if (maxId != null) 'max_id': maxId,
      }).then(
        (json) => SearchResult.fromJson(json),
      );

  Future<List<Hashtag>> getTrends({int limit = 10}) =>
      request(RequestType.get, '/api/v1/trends',
          query: {'limit': limit.toString()}).then(
        (json) =>
            (json['chunk'] as List).map((j) => Hashtag.fromJson(j)).toList(),
      );

  Future<List<Account>> getTrendAccounts({int limit = 10}) =>
      request(RequestType.get, '/api/v1/directory',
          query: {'limit': limit.toString()}).then(
        (json) =>
            (json['chunk'] as List).map((j) => Account.fromJson(j)).toList(),
      );

  Future<Status> favoriteStatus(String statusId) => request(
        RequestType.post,
        '/api/v1/statuses/${Uri.encodeComponent(statusId)}/favourite',
      ).then((json) => Status.fromJson(json));

  Future<Status> unfavoriteStatus(String statusId) => request(
        RequestType.post,
        '/api/v1/statuses/${Uri.encodeComponent(statusId)}/unfavourite',
      ).then((json) => Status.fromJson(json));

  Future<Status> boostStatus(String statusId) => request(
        RequestType.post,
        '/api/v1/statuses/${Uri.encodeComponent(statusId)}/reblog',
      ).then((json) => Status.fromJson(json));

  Future<Status> unboostStatus(String statusId) => request(
        RequestType.post,
        '/api/v1/statuses/${Uri.encodeComponent(statusId)}/unreblog',
      ).then((json) => Status.fromJson(json));

  Future<Relationships> follow(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/follow',
      ).then((json) => Relationships.fromJson(json));

  Future<Relationships> unfollow(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/unfollow',
      ).then((json) => Relationships.fromJson(json));

  Future<Relationships> mute(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/mute',
      ).then((json) => Relationships.fromJson(json));

  Future<Relationships> unmute(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/unmute',
      ).then((json) => Relationships.fromJson(json));

  Future<Relationships> block(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/block',
      ).then((json) => Relationships.fromJson(json));

  Future<Relationships> unblock(String id) => request(
        RequestType.post,
        '/api/v1/accounts/${Uri.encodeComponent(id)}/unblock',
      ).then((json) => Relationships.fromJson(json));

  Future<Status> publishNewStatus({
    String? status,
    List<String>? mediaIds,
    String? inReplyTo,
    bool? sensitive,
    String? spoiler,
    String? language,
    StatusVisibility? visibility,
  }) =>
      request(
        RequestType.post,
        '/api/v1/statuses',
        data: {
          if (status != null) 'status': status,
          if (mediaIds != null) 'media_ids': mediaIds,
          if (inReplyTo != null) 'in_reply_to_id': inReplyTo,
          if (sensitive != null) 'sensitive': sensitive,
          if (spoiler != null) 'spoiler_text': spoiler,
          if (language != null) 'language': language,
          if (visibility != null)
            'visibility': visibility.toString().split('.').last,
        },
      ).then((json) => Status.fromJson(json));

  Future<void> deleteStatus(
    String statusId,
  ) =>
      request(
        RequestType.delete,
        '/api/v1/statuses/${Uri.encodeComponent(statusId)}',
      ).then((json) => Status.fromJson(json));

  Future<PushSubscription?> getCurrentPushSubscription() => request(
        RequestType.get,
        '/api/v1/push/subscription',
      ).then((json) => json['error'] == 'Record not found'
          ? null
          : PushSubscription.fromJson(json));

  Future<PushSubscription> setPushSubcription(
          String endpoint, String p256dh, String auth,
          {PushSubscriptionAlerts? alerts}) =>
      request(
        RequestType.post,
        '/api/v1/push/subscription',
        data: {
          'subscription': {
            'endpoint': endpoint,
            'keys': {
              'p256dh': p256dh,
              'auth': auth,
            },
          },
          if (alerts != null) 'data': {'alerts': alerts.toJson()},
        },
      ).then((json) => PushSubscription.fromJson(json));

  Future<PushSubscription> setPushsubcriptionAlerts(
          PushSubscriptionAlerts alerts) =>
      request(
        RequestType.put,
        '/api/v1/push/subscription',
        data: {
          'data': {'alerts': alerts.toJson()}
        },
      ).then((json) => PushSubscription.fromJson(json));

  Future<ReadMarkers> getMarkers(String timeline) =>
      request(RequestType.get, '/api/v1/markers', query: {
        'timeline': 'notifications',
      }).then((json) => ReadMarkers.fromJson(json));

  Future<ReadMarkers> setMarkers(ReadMarkers readMarkers) => request(
        RequestType.post,
        '/api/v1/markers',
        data: readMarkers.toJson(),
      ).then((json) => ReadMarkers.fromJson(json));

  Future<List<Account>> statusFavouritedBy(String statusId) => request(
        RequestType.get,
        '/api/v1/statuses/$statusId/favourited_by',
      ).then(
        (json) => (json['chunk'] as List)
            .map((json) => Account.fromJson(json))
            .toList(),
      );

  Future<List<Account>> statusRebloggedBy(String statusId) => request(
        RequestType.get,
        '/api/v1/statuses/$statusId/reblogged_by',
      ).then(
        (json) => (json['chunk'] as List)
            .map((json) => Account.fromJson(json))
            .toList(),
      );

  // Update Media Attachment
  Future<MediaAttachment> updateMediaAttachment(String id,
          {String? description}) =>
      request(
        RequestType.put,
        '/api/v1/media/$id',
        data: {
          'description': description,
        },
      ).then((json) => MediaAttachment.fromJson(json));

  Future<Preferences> getAccountPreferences() => request(
        RequestType.get,
        '/api/v1/preferences',
      ).then((json) => Preferences.fromJson(json));

  Future<List<Emoji>> getCustomEmojis() => request(
        RequestType.get,
        '/api/v1/custom_emojis',
      ).then(
        (json) => (json['chunk'] as List)
            .map((json) => Emoji.fromJson(json))
            .toList(),
      );

  Future<MastodonInstance> getInstanceInfo() => request(
        RequestType.get,
        '/api/v1/instance',
      ).then((json) => MastodonInstance.fromJson(json));
}
