// ignore_for_file: unused_import

import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart';

import 'package:fuffle/model/fuffle/api/api.dart';
import '../../mastodon/media_attachment.dart';
import '../fuffle.dart';

extension FuffleUploadExtension on FuffleAPI {
  Future<MediaAttachment> upload(Uint8List bytes, String filename) async {
    final request = MultipartRequest(
      'POST',
      Uri.parse(currentAccount.instanceURL).resolveUri(
        Uri(path: '/api/v1/media'),
      ),
    );
    request.files.add(
      MultipartFile.fromBytes('file', bytes,
          filename: filename.split("/").last),
    );
    request.headers['Authorization'] =
        'Bearer ${currentAccount.accessTokenCredentials.accessToken}';

    final streamedResponse = await request.send();
    var respBody = await streamedResponse.stream.bytesToString();
    return MediaAttachment.fromJson(jsonDecode(respBody));
  }
}
