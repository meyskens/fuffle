import 'dart:convert';

import 'package:http/http.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/model/database/account.dart';

enum RequestType { get, post, put, delete }

class FuffleAPI {
  final Client _client;
  final FuffleAccount currentAccount;

  FuffleAPI(
    this.currentAccount, {
    Client? httpClient,
  }) : _client = httpClient ?? Client();

  Future<Map<String, dynamic>> request(
    RequestType type,
    String action, {
    dynamic data = '',
    int? timeout,
    String contentType = 'application/json',
    String? contentLength,
    String? instanceOverride,
    Map<String, dynamic>? query,
  }) async {
    final instance = Uri.parse(instanceOverride ?? currentAccount.instanceURL);
    dynamic json;
    (data is! String) ? json = jsonEncode(data) : json = data;
    if (data is List<int> || action == '/api/v1/media') json = data;

    final url = instance.resolveUri(Uri(
      path: action,
      queryParameters: query,
    ));

    final headers = <String, String>{};
    if (type == RequestType.put || type == RequestType.post) {
      headers['Content-Type'] = contentType;
      if (contentLength != null) {
        headers['Content-Length'] = contentLength;
      }
    }
    final accessToken = currentAccount.accessTokenCredentials.accessToken;
    if (accessToken != null) {
      headers['Authorization'] = 'Bearer $accessToken';
    }

    Response resp;
    var jsonResp = <String, dynamic>{};
    switch (type) {
      case RequestType.get:
        resp = await _client.get(url, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.post:
        resp = await _client.post(url, body: json, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.put:
        resp = await _client.put(url, body: json, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.delete:
        resp = await _client.delete(url, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
    }
    var respBody = resp.body;
    try {
      respBody = utf8.decode(resp.bodyBytes);
    } catch (_) {
      // No-OP
    }
    if (resp.statusCode >= 500 && resp.statusCode < 600) {
      throw Exception(respBody);
    }
    var jsonString = String.fromCharCodes(respBody.runes);
    if (jsonString.startsWith('[') && jsonString.endsWith(']')) {
      final linkHeader = resp.headers['link'];
      String? next;
      String? prev;
      if (linkHeader != null) {
        final linkHeaderParts = linkHeader.split(',');
        for (final part in linkHeaderParts) {
          if (part.endsWith('rel="next"')) {
            next = Uri.parse(part.split('<').last.split('>').first)
                .queryParameters['max_id'];
          } else if (part.endsWith('rel="prev"')) {
            prev = Uri.parse(part.split('<').last.split('>').first)
                .queryParameters['max_id'];
          }
        }
      }
      // if next and prev are not integers (eg. in iceshrimp) we should quote them
      if (next != null && int.tryParse(next) == null) {
        next = '"$next"';
      }
      if (prev != null && int.tryParse(prev) == null) {
        prev = '"$prev"';
      }
      jsonString = '{"chunk":$jsonString,"next":$next,"prev":$prev}';
    }
    try {
      jsonResp = jsonDecode(jsonString) as Map<String, dynamic>;
    } catch (e) {
      throw throw ServerErrorResponse(resp);
    }
    if (resp.statusCode >= 400 && resp.statusCode < 500) {
      throw ServerErrorResponse(resp);
    }

    return jsonResp;
  }
}

class ServerErrorResponse implements Exception {
  final Response response;

  ServerErrorResponse(this.response);

  @override
  String toString() {
    try {
      final json = jsonDecode(response.body) as Map<String, dynamic>;
      return json['error_description'] ?? json['error'];
    } catch (_) {}
    if (response.reasonPhrase?.isNotEmpty ?? false) {
      return response.reasonPhrase!;
    }
    return 'Server responded with status code ${response.statusCode}';
  }
}
