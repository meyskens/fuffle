import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/config/instances_api_token.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/token.dart';
import 'package:fuffle/model/instances/public_instance.dart';
import 'api/api.dart';
import 'fuffle.dart';

class FuffleLogin {
  static final FuffleLogin _singleton = FuffleLogin._internal();

  factory FuffleLogin() {
    return _singleton;
  }

  FuffleLogin._internal();

  String get _redirectUri =>
      _noRedirectUri; // TODO: add support for app redirection again, this was removed for bugs on android

  static const String _noRedirectUri = 'urn:ietf:wg:oauth:2.0:oob';

  String? newInstance;

  bool get automaticRedirectUriAvailable => _redirectUri != _noRedirectUri;

  Uri getOAuthUri(
    String domain,
    String clientId,
  ) =>
      Uri.https(domain, '/oauth/authorize', {
        'client_id': clientId,
        'redirect_uri': _redirectUri,
        'response_type': 'code',
        'scope': 'read write follow push',
      });

  static const String _defaultSearchLanguage = 'en';

  Future<Map<String, dynamic>> request(
    RequestType type,
    String action, {
    dynamic data = '',
    int? timeout,
    String contentType = 'application/json',
    String? contentLength,
    required String instanceOverride,
    Map<String, dynamic>? query,
  }) async {
    final instance = Uri.parse(instanceOverride);
    dynamic json;
    (data is! String) ? json = jsonEncode(data) : json = data;

    final url = instance.resolveUri(Uri(
      path: action,
      queryParameters: query,
    ));

    final headers = <String, String>{};
    if (type == RequestType.put || type == RequestType.post) {
      headers['Content-Type'] = contentType;
      if (contentLength != null) {
        headers['Content-Length'] = contentLength;
      }
    }

    Response resp;
    var jsonResp = <String, dynamic>{};
    switch (type) {
      case RequestType.get:
        resp = await Client().get(url, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.post:
        resp = await Client().post(url, body: json, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.put:
        resp = await Client().put(url, body: json, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
      case RequestType.delete:
        resp = await Client().delete(url, headers: headers).timeout(
              AppConfigs.defaultTimeout,
            );
        break;
    }
    var respBody = resp.body;
    try {
      respBody = utf8.decode(resp.bodyBytes);
    } catch (_) {
      // No-OP
    }
    if (resp.statusCode >= 500 && resp.statusCode < 600) {
      throw Exception(respBody);
    }
    var jsonString = String.fromCharCodes(respBody.runes);
    if (jsonString.startsWith('[') && jsonString.endsWith(']')) {
      final linkHeader = resp.headers['link'];
      String? next;
      String? prev;
      if (linkHeader != null) {
        final linkHeaderParts = linkHeader.split(',');
        for (final part in linkHeaderParts) {
          if (part.endsWith('rel="next"')) {
            next = Uri.parse(part.split('<').last.split('>').first)
                .queryParameters['max_id'];
          } else if (part.endsWith('rel="prev"')) {
            prev = Uri.parse(part.split('<').last.split('>').first)
                .queryParameters['max_id'];
          }
        }
      }
      jsonString = '{"chunk":$jsonString,"next":$next,"prev":$prev}';
    }
    try {
      jsonResp = jsonDecode(jsonString) as Map<String, dynamic>;
    } catch (_) {
      throw throw ServerErrorResponse(resp);
    }
    if (resp.statusCode >= 400 && resp.statusCode < 500) {
      throw ServerErrorResponse(resp);
    }

    return jsonResp;
  }

  Future<CreateApplicationResponse> createApplication(
    String clientName,
    String instanceURL,
    String redirectUris, {
    String? scopes,
    String? website,
  }) =>
      request(RequestType.post, '/api/v1/apps',
              data: {
                'client_name': clientName,
                'redirect_uris': redirectUris,
                if (scopes != null) 'scopes': scopes,
                if (website != null) 'website': website,
              },
              instanceOverride: instanceURL)
          .then((json) => CreateApplicationResponse.fromJson(json));

  Future<CreateApplicationResponse> connectToInstance(
      String domain, void Function(Uri) launch) async {
    final instance = Uri.https(domain, '/');
    newInstance = instance.toString();

    debugPrint('Create Application on $instance...');

    CreateApplicationResponse? createApplicationResponse;

    createApplicationResponse = await CreateApplicationResponse.getForInstance(
        instance.toString()); // check if we already have a token
    createApplicationResponse ??= await createApplication(
      AppConfigs.applicationName,
      instance.toString(),
      _redirectUri,
      scopes: 'read write follow push',
      website: AppConfigs.applicationWebsite,
    );

    final oAuthUri = getOAuthUri(
      domain,
      createApplicationResponse.clientId,
    );
    debugPrint('Open OAuth Uri $oAuthUri...');
    launch(oAuthUri);

    await CreateApplicationResponse.store(
        instance.toString(), createApplicationResponse);

    return createApplicationResponse;
  }

  Future<AccessTokenCredentials> obtainToken(
    String clientId,
    String clientSecret,
    String instanceURL,
    String redirectUri, {
    String? scope,
    String? code,
    String? grantType,
  }) =>
      request(RequestType.post, '/oauth/token',
              data: {
                'client_id': clientId,
                'client_secret': clientSecret,
                'redirect_uri': redirectUri,
                if (scope != null) 'scope': scope,
                if (code != null) 'code': code,
                if (grantType != null) 'grant_type': grantType,
              },
              instanceOverride: instanceURL)
          .then(
        (json) => AccessTokenCredentials.fromJson(json),
      );

  Future<void> revokeToken(
    String clientId,
    String clientSecret,
    String token,
    String instanceURL,
  ) =>
      request(RequestType.post, '/oauth/revoke',
          data: {
            'client_id': clientId,
            'client_secret': clientSecret,
            'token': token,
          },
          instanceOverride: instanceURL);

  Future<int> login(
    BuildContext context,
    String code,
    CreateApplicationResponse createApplicationResponse,
  ) async {
    final fuffle = Fuffle.of(context);
    final accountDB = fuffle.accountDB;

    if (newInstance == null) throw Exception('Connect to instance first!');

    final accessTokenCredentials = await obtainToken(
      createApplicationResponse.clientId,
      createApplicationResponse.clientSecret,
      newInstance!,
      _redirectUri,
      code: code,
      grantType: 'authorization_code',
      scope: 'read write follow push',
    );
    final newAccount = FuffleAccount(
        accessTokenCredentials: accessTokenCredentials,
        instanceURL: newInstance!);
    await newAccount.updateOwnAccount();
    final newID = accountDB.length;
    await accountDB.put(newID, newAccount);
    newInstance = null;
    fuffle.currentAccountIndex = newID;

    return newID;
  }

  Future<List<PublicInstance>> requestInstances(String language,
      [String? query]) async {
    if (query?.isEmpty ?? false) query = null;
    final url = Uri.https(
      'instances.social',
      '/api/1.0/instances/${query == null ? 'list' : 'search'}',
      query != null
          ? {'q': query}
          : {
              'language': language,
              'prohibited_content': 'nudity_all',
              'sort_by': 'active_users',
              'sort_order': 'desc',
            },
    );
    final response = await get(
      url,
      headers: {'Authorization': 'Bearer ${InstancesApiToken.token}'},
    );
    if (response.statusCode != 200) throw Exception(response.reasonPhrase);
    final responseJson = jsonDecode(response.body);
    final instances = (responseJson['instances'] as List)
        .map((json) => PublicInstance.fromJson(json))
        .toList();
    if (query == null &&
        instances.isEmpty &&
        language != _defaultSearchLanguage) {
      return requestInstances(_defaultSearchLanguage);
    }
    if (query == null) {
      instances.removeWhere((i) => i.openRegistrations != true);
    }
    return instances;
  }
}
