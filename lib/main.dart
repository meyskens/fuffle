import 'package:flutter/material.dart';

import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter_fgbg/flutter_fgbg.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_langdetect/flutter_langdetect.dart' as langdetect;
import 'package:hive_flutter/hive_flutter.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/config/app_routes.dart';
import 'package:fuffle/config/app_themes.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/widgets/core/theme_builder.dart';
import 'model/database/init.dart';

void main() async {
  await Hive.initFlutter();
  initAdapters();

  WidgetsFlutterBinding.ensureInitialized();

  final fuffle = Fuffle();
  await fuffle.init();
  await langdetect.initLangDetect();
  runApp(FuffleApp(fuffle: fuffle));
}

class FuffleApp extends StatelessWidget {
  final Fuffle fuffle;
  const FuffleApp({super.key, required this.fuffle});

  @override
  Widget build(BuildContext context) {
    return FGBGNotifier(
      onEvent: (event) {
        fuffle.isInBackground = event == FGBGType.background;
      },
      child: ThemeBuilder(
        builder: (context, themeMode, color) => DynamicColorBuilder(
          builder: (light, dark) => MaterialApp.router(
            title: AppConfigs.applicationName,
            themeMode: themeMode,
            theme: AppThemes.buildTheme(light, color, true),
            darkTheme: AppThemes.buildTheme(dark, color, false),
            builder: fuffle.builder,
            routerConfig: goRouter,
            localizationsDelegates: L10n.localizationsDelegates,
            supportedLocales: L10n.supportedLocales,
          ),
        ),
      ),
    );
  }
}
