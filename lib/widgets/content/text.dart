import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';

import 'package:fuffle/model/database/mastodon/status.dart';

class FuffleText extends StatelessWidget {
  final String data;
  final List<Emoji>? emojis;
  final TextStyle? style;

  final List<Widget> _children = [];

  FuffleText(this.data, {super.key, this.emojis, this.style}) {
    // split data into parts and fill :shortcode: with emojis
    final regex = RegExp(r':(\w+):');
    final matches = regex.allMatches(data).toList();

    int start = 0;
    for (var match in matches) {
      if (start != match.start) {
        _children.add(Text(data.substring(start, match.start), style: style));
      }

      _children.add(_emoji(match.group(1)!));
      start = match.end;
    }

    // Add the remaining text part after the last emoji shortcode.
    if (start != data.length) {
      _children.add(Text(data.substring(start, data.length), style: style));
    }
  }

  Widget _emoji(String shortcode) {
    if (emojis == null) {
      return Text(':$shortcode:', style: style);
    }

    final emoji =
        emojis!.firstWhereOrNull((element) => element.shortcode == shortcode);
    if (emoji == null) {
      return Text(':$shortcode:', style: style);
    }
    var height = style?.fontSize ?? 16.0;
    height = height * 1.2;

    return Padding(
        padding: const EdgeInsets.only(left: 1, right: 1),
        child: CachedNetworkImage(
          imageUrl: emoji.url,
          height: height,
          errorWidget: (_, __, ___) => Text(':$shortcode:', style: style),
          placeholder: (_, __) => SizedBox(height: height, width: height),
        ));
  }

  @override
  Widget build(BuildContext context) => Wrap(children: _children);
}
