import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/utils/links_callback.dart';

class FuffleHTML extends StatelessWidget {
  final String data;
  final List<Emoji>? emojis;
  final Map<String, Style> style;

  String _replaceShortcodesWithEmojiTags(String input) {
    final regex = RegExp(r':(\w+):');
    return input.replaceAllMapped(regex, (match) {
      return '<emoji shortcode="${match.group(1)}"></emoji>';
    });
  }

  const FuffleHTML(
      {super.key, required this.data, this.emojis, this.style = const {}});

  Widget _emoji(ExtensionContext ctx) {
    if (emojis == null) {
      return Text(':${ctx.attributes['shortcode']!}:');
    }

    final emoji = emojis!.firstWhereOrNull(
        (element) => element.shortcode == ctx.attributes['shortcode']);

    if (emoji == null) {
      return Text(':${ctx.attributes['shortcode']!}:');
    }

    var height = ctx.style?.fontSize?.value ?? 16.0;
    return Padding(
      padding: const EdgeInsets.only(left: 1, right: 1),
      child: CachedNetworkImage(
        imageUrl: emoji.url,
        height: height * 1.2,
        errorWidget: (_, __, ___) => Text(':${ctx.attributes['shortcode']!}:'),
        placeholder: (_, __) => const Text(' '),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Html(
      data: _replaceShortcodesWithEmojiTags(data),
      onLinkTap: (url, attributes, element) =>
          linksCallback(url.toString(), context), // TODO: make URL a parsed URL
      extensions: [
        TagExtension(
          tagsToExtend: {"emoji"},
          builder: _emoji,
        ),
      ],
      style: {"a": Style(textDecoration: TextDecoration.none), ...style},
    );
  }
}
