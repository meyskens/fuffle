import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/mastodon/notification.dart';
import 'package:fuffle/utils/links_callback.dart';
import 'package:fuffle/widgets/accounts/avatar.dart';
import '../../utils/date_time_extension.dart';

class NotificationWidget extends StatefulWidget {
  final MastodonNotification notification;

  const NotificationWidget({
    super.key,
    required this.notification,
  });

  @override
  NotificationWidgetState createState() => NotificationWidgetState();
}

class NotificationWidgetState extends State<NotificationWidget> {
  void openPostAction() => context.push(
        '/status/${widget.notification.status?.id}',
      );

  void openProfileAction() => context.push(
        '/user/${widget.notification.account?.id}',
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: InkWell(
              borderRadius: BorderRadius.circular(64),
              onTap: openProfileAction,
              child: Avatar(account: widget.notification.account!)),
          title: Flex(
            direction: Axis.horizontal,
            children: [
              Icon(
                widget.notification.iconData,
                size: 18,
                color: widget.notification.color,
              ),
              Flexible(
                  child: Html(
                data: widget.notification.toLocalizedString(context),
              )),
              Text(
                widget.notification.createdAt.localizedTimeShort(context),
                style: const TextStyle(fontSize: 12),
              ),
            ],
          ),
          subtitle: widget.notification.status != null
              ? Row(
                  children: [
                    Expanded(
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  const SizedBox(width: 8),
                                  Avatar(
                                      account:
                                          widget.notification.status!.account!,
                                      radius: 8),
                                  const SizedBox(width: 8),
                                  Text(
                                    widget.notification.status!.account!
                                        .calcedDisplayname,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Html(
                                  data:
                                      widget.notification.status!.content ?? '',
                                  onLinkTap: (url, attributes, element) =>
                                      linksCallback(url.toString(), context),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Container(),
        ),
        const Divider(height: 1),
      ],
    );
  }
}

extension on MastodonNotification {
  String toLocalizedString(BuildContext context) {
    switch (type) {
      case NotificationType.mention:
        return L10n.of(context)!.mentioned(account!.calcedDisplayname);
      case NotificationType.favourite:
        return L10n.of(context)!.likesYourPost(account!.calcedDisplayname);
      case NotificationType.follow:
        return L10n.of(context)!.isFollowingYouNow(account!.calcedDisplayname);
      case NotificationType.follow_request:
        return L10n.of(context)!
            .wouldLikeToFollowYou(account!.calcedDisplayname);
      case NotificationType.reblog:
        return L10n.of(context)!.sharedYourPost(account!.calcedDisplayname);
      case NotificationType.poll:
        return L10n.of(context)!.pollEnded;
      case NotificationType.status:
        return L10n.of(context)!.hasPosted(account!.calcedDisplayname);
      case NotificationType.update:
        return "update";
      case NotificationType.unknown:
        return "unknown";
    }
  }

  Color? get color {
    switch (type) {
      case NotificationType.mention:
        return Colors.orange[700];
      case NotificationType.favourite:
        return Colors.red[700];
      case NotificationType.reblog:
        return Colors.green[700];
      case NotificationType.follow:
        return Colors.blue[700];
      case NotificationType.follow_request:
      case NotificationType.poll:
      case NotificationType.status:
      case NotificationType.update:
      case NotificationType.unknown:
        return null;
    }
  }

  IconData get iconData {
    switch (type) {
      case NotificationType.mention:
        return Icons.chat_bubble;
      case NotificationType.favourite:
        return Icons.favorite;
      case NotificationType.follow:
        return Icons.person_add_alt_1;
      case NotificationType.follow_request:
        return Icons.person_search_sharp;
      case NotificationType.reblog:
        return Icons.repeat;
      case NotificationType.poll:
        return Icons.poll;
      case NotificationType.status:
        return Icons.notifications;
      case NotificationType.update:
        return Icons.edit;
      case NotificationType.unknown:
        return Icons.error;
    }
  }
}
