import 'package:flutter/material.dart';

import 'package:flutter_blurhash/flutter_blurhash.dart';

import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/widgets/content/html.dart';
import 'package:fuffle/widgets/content/text.dart';
import 'package:fuffle/widgets/status/status_content_grid.dart';
import '../../config/app_configs.dart';

enum StatusMode { timeline, reply }

class StatusContent extends StatefulWidget {
  final Status status;
  final StatusMode statusMode;
  final bool expanded;
  final Function? setExpanded;

  const StatusContent({
    required this.status,
    this.statusMode = StatusMode.timeline,
    this.expanded = false,
    this.setExpanded,
    super.key,
  });

  @override
  StatusContentState createState() => StatusContentState();
}

class StatusContentState extends State<StatusContent> {
  bool get _expanded => widget.expanded;
  bool _unlocked = false;

  void onUnlock() {
    setState(() {
      _unlocked = !_unlocked;
      if (widget.setExpanded != null && _expanded) {
        widget.setExpanded!(_unlocked);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final contentStatus = widget.status.reblog ?? widget.status;
    if (contentStatus.needsContentWarning && _expanded) _unlocked = _expanded;
    final hide = contentStatus.needsContentWarning && !_unlocked;

    Widget? mediaContent;
    if (contentStatus.mediaAttachments!.isNotEmpty && !hide) {
      mediaContent = StatusContentMedia(
        status: contentStatus,
        imageStatusMode: widget.statusMode,
      );
    } else if (contentStatus.mediaAttachments!.isNotEmpty && hide) {
      final height = contentStatus
              .mediaAttachments!.first.imageMeta.small?.height ??
          contentStatus.mediaAttachments!.first.imageMeta.original?.height ??
          200;
      mediaContent = SizedBox(
        height: height.toDouble(),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                child: BlurHash(
                  hash: contentStatus.mediaAttachments!.isEmpty
                      ? AppConfigs.fallbackBlurHash
                      : contentStatus.mediaAttachments!.first.blurhash ??
                          AppConfigs.fallbackBlurHash,
                ),
              ),
            ),
            Center(
              child: SizedBox(
                height: 48,
                child: FilledButton.tonal(
                  onPressed: onUnlock,
                  child: const Text(
                    "Open",
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (contentStatus.needsContentWarning)
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      const Icon(Icons.warning_amber_rounded),
                      FuffleText(
                        contentStatus.spoilerText.isNotEmpty
                            ? contentStatus.spoilerText
                            : contentStatus.content ?? "",
                        style: const TextStyle(fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
                if ((mediaContent == null && hide) || !hide)
                  SizedBox(
                      width: double.infinity,
                      child: FilledButton.tonal(
                          onPressed: onUnlock,
                          child: Text(_unlocked ? "Close" : "Open")))
              ],
            ),
          ),
        if (!hide)
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: FuffleHTML(
              data: contentStatus.content ?? '',
              emojis: contentStatus.emojis,
            ),
          ),
        if (mediaContent != null)
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: mediaContent,
          ),
        const Divider(height: 1, thickness: 1),
      ],
    );
  }
}
