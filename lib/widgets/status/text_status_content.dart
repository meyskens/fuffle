import 'dart:math';

import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';

import 'package:fuffle/config/app_themes.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/widgets/status/status_content.dart';

enum ImageType { image, avatar, missing }

// TODO: fully rewrite

class TextStatusContent extends StatelessWidget {
  final Status status;
  final StatusMode imageStatusMode;

  const TextStatusContent({
    super.key,
    required this.status,
    required this.imageStatusMode,
  });

  ImageType get _type {
    if ((status.mediaAttachments!.isNotEmpty &&
        !status.mediaAttachments!.first.url
            .toString()
            .endsWith('missing.png'))) {
      return ImageType.image;
    }
    if (status.account!.headerStatic.isNotEmpty &&
            !status.account!.headerStatic.endsWith('missing.png') ||
        status.account!.avatarStatic.isNotEmpty) {
      return ImageType.avatar;
    }
    return ImageType.missing;
  }

  @override
  Widget build(BuildContext context) {
    if (_type == ImageType.missing) {
      return Center(
          child: Image.asset(
        'assets/logo/fuffle.png',
        width: 56,
        height: 56,
      ));
    }
    if (_type != ImageType.missing &&
        (imageStatusMode != StatusMode.reply || _type == ImageType.image)) {
      final width = AppThemes.isColumnMode(context)
          ? AppThemes.mainColumnWidth
          : MediaQuery.of(context).size.width;

      if (status.card?.image == null) {
        return Container();
      }

      return CachedNetworkImage(
        imageUrl: status.card!.image!,
        width: double.infinity,
        fit: BoxFit.cover,
        height: min(width * 1 / 2, 160),
        placeholder: (_, __) => SizedBox(
          height: width * 1 / 2,
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }
    return Container();
  }
}
