import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:share/share.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/database/mastodon/status_visibility.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/utils/reply.dart';
import 'package:fuffle/widgets/accounts/avatar.dart';
import 'package:fuffle/widgets/content/text.dart';
import 'package:fuffle/widgets/status/status_content.dart';
import '../../model/fuffle/api/api_extension.dart';
import '../../utils/accounts.dart';
import '../../utils/date_time_extension.dart';

class StatusWidget extends StatefulWidget {
  final Status status;
  final bool replyMode;
  final FuffleAccount ownAccount;
  final void Function(Status? status, [String? deleteId]) onUpdate;

  const StatusWidget({
    super.key,
    required this.ownAccount,
    required this.status,
    required this.onUpdate,
    this.replyMode = false,
  });

  @override
  StatusWidgetState createState() => StatusWidgetState();
}

class StatusWidgetState extends State<StatusWidget>
    with AutomaticKeepAliveClientMixin {
  bool _expanded = false;

  bool _liked = false;
  int _likesCount = 0;
  bool _boosted = false;
  int _boostsCount = 0;

  @override
  void initState() {
    super.initState();
    _liked = widget.status.favourited ?? false;
    _likesCount = widget.status.favouritesCount;
    _boosted = widget.status.reblogged ?? false;
    _boostsCount = widget.status.reblogsCount;
  }

  Status get contentStatus => widget.status.reblog ?? widget.status;

  void updateStats(Status s) {
    if (s.reblog != null) {
      s = s.reblog!;
    }
    _liked = s.favourited ?? false;
    _likesCount = s.favouritesCount;
    _boosted = s.reblogged ?? false;
    _boostsCount = s.reblogsCount;
    setState(() {});
  }

  void favoriteAction() async {
    try {
      _liked = !_liked;
      _likesCount += _liked ? 1 : -1;
      setState(() {});

      final api = FuffleAPI(widget.ownAccount);
      final status = await (contentStatus.favourited ?? false
          ? api.unfavoriteStatus(contentStatus.id)
          : api.favoriteStatus(contentStatus.id));

      widget.onUpdate(status);
      updateStats(status);
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    }
  }

  void boostAction() async {
    try {
      _boosted = !_boosted;
      _boostsCount += _boosted ? 1 : -1;
      setState(() {});

      final api = FuffleAPI(widget.ownAccount);
      final status = await (contentStatus.reblogged ?? false
          ? api.unboostStatus(contentStatus.id)
          : api.boostStatus(contentStatus.id));
      widget.onUpdate(status, contentStatus.id);
      updateStats(status);
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    }
  }

  void openPostAction() => context.push(
        '/status/${(contentStatus).id}',
      );

  // TODO: make this open the compose view instead
  void commentAction() {
    if (widget.replyMode) {
      openReplyCompose(context, widget.ownAccount, contentStatus);
      return;
    }
    context.push(
      '/status/${(contentStatus).id}',
    );
  }

  void deleteAction() async {
    final confirmed = await showOkCancelAlertDialog(
      context: context,
      title: L10n.of(context)!.deletePost,
      message: L10n.of(context)!.areYouSure,
      okLabel: L10n.of(context)!.delete,
      isDestructiveAction: true,
      cancelLabel: L10n.of(context)!.cancel,
      fullyCapitalizedForMaterial: false,
    );
    if (confirmed != OkCancelResult.ok) return;
    try {
      if (mounted) {
        final featureController = ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.deleting),
          ),
        );
        await FuffleAPI(widget.ownAccount).deleteStatus(widget.status.id);
        featureController.close();
      }
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.postHasBeenDeleted),
          ),
        );
        widget.onUpdate(null, widget.status.id);
      }
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    }
  }

  void onStatusAction(StatusAction action) {
    switch (action) {
      case StatusAction.shareLink:
        if (!kIsWeb && (Platform.isIOS || Platform.isAndroid)) {
          Share.share(contentStatus.uri);
        } else {
          Clipboard.setData(ClipboardData(text: contentStatus.uri));
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(L10n.of(context)!.copiedToClipboard),
            ),
          );
        }
        break;
      case StatusAction.open:
        commentAction();
        break;
      case StatusAction.report:
        reportAction();
        break;
      case StatusAction.delete:
        deleteAction();
        break;
      case StatusAction.sharedBy:
        context.push('/status/${contentStatus.id}/sharedby');
        break;
      case StatusAction.likedBy:
        context.push('/status/${contentStatus.id}/likedby');
        break;
    }
  }

  void reportAction() async {
    final comment = await showTextInputDialog(
      context: context,
      title: L10n.of(context)!.report,
      message: L10n.of(context)!.reportDescription,
      textFields: [DialogTextField(hintText: L10n.of(context)!.reason)],
    );
    if (comment == null) return;

    if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context)!.loading),
        ),
      );
    }
    try {
      if (mounted) {
        await FuffleAPI(widget.ownAccount).report(
          widget.status.account!.id,
          [widget.status.id],
          comment.first,
        );
      }
    } catch (_) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context)!.oopsSomethingWentWrong),
          ),
        );
      }
      rethrow;
    }
    if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context)!.postHasBeenReported),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // force expanded in reply mode
    if (widget.replyMode) _expanded = true;
    super.build(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // add repost indicator
        if (widget.status.reblog != null)
          Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
              left: 12,
              right: 12,
            ),
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14.0),
                  child: Icon(
                    Icons.repeat,
                    color: Colors.green,
                    size: 20,
                  ),
                ),
                const SizedBox(width: 8),
                Avatar(account: widget.status.account!, radius: 8),
                const SizedBox(width: 4),
                FuffleText(
                  L10n.of(context)!.userShared(
                    displayName(widget.status.account!),
                  ),
                  style: const TextStyle(fontSize: 12),
                  emojis: widget.status.account!.emojis,
                ),
              ],
            ),
          ),
        // user info
        ListTile(
          leading: InkWell(
            borderRadius: BorderRadius.circular(64),
            onTap: () => context.push('/user/${contentStatus.account!.id}'),
            child: Avatar(account: contentStatus.account!),
          ),
          title: Row(
            children: [
              Expanded(
                child: FuffleText(
                  contentStatus.account!.calcedDisplayname,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  emojis: contentStatus.account!.emojis,
                ),
              ),
              Text(
                contentStatus.createdAt.localizedTimeShort(context),
                style: const TextStyle(fontSize: 12),
              ),
            ],
          ),
          subtitle: Row(
            children: [
              Expanded(
                child: Text(
                  '@${contentStatus.account!.acct}',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Row(
                children: [
                  if (contentStatus.inReplyToId != null)
                    const Icon(
                      Icons.reply_rounded,
                      size: 18,
                    ),
                  const SizedBox(width: 8),
                  if (contentStatus.fuffleLanguage != null &&
                      contentStatus.fuffleLanguage!.isNotEmpty)
                    const Icon(
                      Icons.translate,
                      size: 15,
                    ),
                  Text(contentStatus.fuffleLanguage ?? ""),
                  const SizedBox(width: 8),
                  Icon(
                    contentStatus.visibility.icon,
                    size: 16,
                    color: Theme.of(context).textTheme.bodyLarge?.color,
                  ),
                ],
              ),
            ],
          ),
        ),
        InkWell(
          onTap: widget.replyMode
              ? null
              : () => setState(() {
                    _expanded = !_expanded;
                  }),
          onLongPress: widget.replyMode ? null : openPostAction,
          child: StatusContent(
            key: widget.key,
            status: widget.status,
            expanded: _expanded,
            statusMode: widget.status.visibility == StatusVisibility.direct
                ? StatusMode.reply
                : StatusMode.timeline,
            setExpanded: (value) => setState(() => _expanded = value),
          ),
        ),
        if (_expanded)
          // action buttons
          Container(
            color: Theme.of(context).colorScheme.secondary.withAlpha(40),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  IconButton(
                    onPressed: commentAction,
                    icon: Row(
                      children: [
                        const Icon(Icons.chat_bubble),
                        if (contentStatus.repliesCount > 0)
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(contentStatus.repliesCount.toString()),
                          )
                      ],
                    ),
                    tooltip: "reply",
                    padding: const EdgeInsets.only(left: 16, right: 16),
                  ),
                  IconButton(
                    onPressed: favoriteAction,
                    tooltip: "favorite",
                    icon: Row(
                      children: [
                        const Icon(Icons.favorite),
                        if (_likesCount > 0)
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(_likesCount.toString()),
                          )
                      ],
                    ),
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    color: _liked ? Colors.red : null,
                  ),
                  IconButton(
                    onPressed: contentStatus.visibility ==
                                StatusVisibility.direct ||
                            contentStatus.visibility == StatusVisibility.private
                        ? null
                        : boostAction,
                    tooltip: "boost",
                    disabledColor: Theme.of(context).disabledColor,
                    icon: Row(
                      children: [
                        const Icon(Icons.repeat),
                        if (_boostsCount > 0)
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(_boostsCount.toString()),
                          )
                      ],
                    ),
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    color: _boosted ? Colors.green : null,
                  ),
                  const Spacer(),
                  PopupMenuButton<StatusAction>(
                    onSelected: onStatusAction,
                    itemBuilder: (_) => [
                      PopupMenuItem(
                        value: StatusAction.open,
                        child: Text(L10n.of(context)!.viewPost),
                      ),
                      PopupMenuItem(
                        value: StatusAction.report,
                        child: Text(L10n.of(context)!.report),
                      ),
                      PopupMenuItem(
                        value: StatusAction.shareLink,
                        child: Text(L10n.of(context)!.shareLink),
                      ),
                      if (widget.ownAccount.mastodonAccount()!.id ==
                          widget.status.account!.id)
                        PopupMenuItem(
                          value: StatusAction.delete,
                          child: Text(
                            L10n.of(context)!.delete,
                            style: const TextStyle(color: Colors.red),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

enum StatusAction { open, report, delete, shareLink, sharedBy, likedBy }
