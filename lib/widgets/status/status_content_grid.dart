// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';

import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/widgets/status/attachment_viewer.dart';
import 'package:fuffle/widgets/status/status_content.dart';

class StatusContentMedia extends StatefulWidget {
  final StatusMode imageStatusMode;
  final Status status;
  const StatusContentMedia({
    required this.status,
    required this.imageStatusMode,
    super.key,
  });

  @override
  StatusContentMediaState createState() => StatusContentMediaState();
}

class StatusContentMediaState extends State<StatusContentMedia> {
  final PageController pageController = PageController();

  num get currentPage =>
      (pageController.hasClients ? pageController.page ?? 0 : 0);

  @override
  void initState() {
    pageController.addListener(() => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    pageController.removeListener(() => setState(() {}));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const rad = Radius.circular(15);

    if (widget.status.mediaAttachments!.length == 1) {
      const br = BorderRadius.all(rad);
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Material(
          elevation: 1,
          borderOnForeground: true,
          borderRadius: br,
          child: ClipRRect(
            borderRadius: br,
            child: AttachmentViewer(
              attachment: widget.status.mediaAttachments!.single,
              isSingleImage: true,
              fit: BoxFit.cover, // this will cut only a few pixels off
            ),
          ),
        ),
      );
    }

    return LayoutBuilder(builder: (context, constraints) {
      return Wrap(
        spacing: 5.0, // Spacing between columns
        runSpacing: 5.0, // Spacing between rows
        children: widget.status.mediaAttachments!.map((item) {
          final i = widget.status.mediaAttachments!.indexOf(item);
          final isLastFullWidth =
              widget.status.mediaAttachments!.length % 2 != 0 &&
                  i == widget.status.mediaAttachments!.length - 1;
          final isSingleRow = widget.status.mediaAttachments!.length == 2;
          final isLeft = i % 2 == 0;
          final isFirstRow = i < 2;
          final isLastRow = widget.status.mediaAttachments!.length - i < 3;

          late final BorderRadius br;
          if (isLastFullWidth)
            br = const BorderRadius.only(bottomLeft: rad, bottomRight: rad);
          else if (isLeft && isSingleRow)
            br = const BorderRadius.only(topLeft: rad, bottomLeft: rad);
          else if (!isLeft && isSingleRow)
            br = const BorderRadius.only(topRight: rad, bottomRight: rad);
          else if (isLeft && !isSingleRow && isFirstRow)
            br = const BorderRadius.only(topLeft: rad);
          else if (!isLeft && !isSingleRow && isFirstRow)
            br = const BorderRadius.only(topRight: rad);
          else if (isLeft && !isSingleRow && isLastRow)
            br = const BorderRadius.only(bottomLeft: rad);
          else if (!isLeft && !isSingleRow && isLastRow)
            br = const BorderRadius.only(bottomRight: rad);
          else
            br = const BorderRadius.only();

          return SizedBox(
            // set to half width unless it is uneven then make the last full width
            width: isLastFullWidth
                ? constraints.maxWidth - 25
                : constraints.maxWidth / 2 - 15, // 2 columns
            child: Center(
                child: SizedBox(
              height: 200,
              child: Material(
                elevation: 1,
                borderOnForeground: true,
                borderRadius: br,
                child: ClipRRect(
                  borderRadius: br,
                  child: AttachmentViewer(
                    attachment: item,
                    fit: BoxFit.cover,
                    allMedia: widget.status.mediaAttachments,
                  ),
                ),
              ),
            )),
          );
        }).toList(),
      );
    });
  }
}
