import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:video_player/video_player.dart';

import 'package:fuffle/config/app_configs.dart';
import 'package:fuffle/config/app_themes.dart';
import 'package:fuffle/model/mastodon/media_attachment.dart';
import 'package:fuffle/utils/links_callback.dart';
import '../../pages/image_viewer/image_viewer.dart';

class AttachmentViewer extends StatelessWidget {
  final MediaAttachment attachment;
  final BoxFit? fit;
  final List<MediaAttachment>? allMedia;
  final bool isSingleImage;

  const AttachmentViewer({
    required this.attachment,
    this.fit,
    this.allMedia,
    this.isSingleImage = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    switch (attachment.type) {
      case MediaType.image:
        return _AttachmentImageViewer(
          attachment: attachment,
          fit: fit,
          allMedia: allMedia,
          heightLimitImage: isSingleImage,
        );
      case MediaType.video:
      case MediaType.gifv:
      case MediaType.audio:
        if (kIsWeb || Platform.isAndroid || Platform.isIOS) {
          return _AttachVideoViewer(
            attachment: attachment,
          );
        }
        return _PlayInBrowserButton(
          attachment: attachment,
        );
      case MediaType.unknown:
        return _PlayInBrowserButton(
          attachment: attachment,
        );
    }
  }
}

class _AttachVideoViewer extends StatefulWidget {
  final MediaAttachment attachment;
  const _AttachVideoViewer({
    required this.attachment,
  });

  @override
  __AttachVideoViewerState createState() => __AttachVideoViewerState();
}

class __AttachVideoViewerState extends State<_AttachVideoViewer> {
  late final VideoPlayerController _videoPlayerController;
  FlickManager? _flickManager;

  @override
  void initState() {
    super.initState();
    _videoPlayerController =
        VideoPlayerController.networkUrl(widget.attachment.url);
    _videoPlayerController.initialize().then((_) {
      setState(() {
        _flickManager = FlickManager(
          videoPlayerController: _videoPlayerController,
          autoPlay: false,
        );
        _flickManager!.flickControlManager!.mute();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final flickManager = _flickManager;
    if (flickManager == null) {
      return _AttachmentImageViewer(
        attachment: widget.attachment,
        heightLimitImage: true,
      );
    }
    double? width = AppThemes.isColumnMode(context)
        ? AppThemes.columnWidth * 2
        : MediaQuery.of(context).size.width;
    double? height = widget.attachment.type == MediaType.audio
        ? 168.0
        : widget.attachment.videoMeta.small?.aspect == null
            ? width
            : width / widget.attachment.videoMeta.small!.aspect!;
    if (height > MediaQuery.of(context).size.height / 2) {
      height = MediaQuery.of(context).size.height / 2;
      width = null;
    }
    return SizedBox(
      width: width,
      height: height,
      child: FlickVideoPlayer(flickManager: flickManager),
    );
  }
}

class _AttachmentImageViewer extends StatelessWidget {
  final MediaAttachment attachment;
  final BoxFit? fit;
  final List<MediaAttachment>? allMedia;
  final bool heightLimitImage;

  const _AttachmentImageViewer({
    required this.attachment,
    this.fit,
    this.allMedia,
    this.heightLimitImage = false,
  });

  void onOpen(BuildContext context) {
    late final MultiImageProvider multiImageProvider;
    if (allMedia == null) {
      multiImageProvider = MultiImageProvider([
        CachedNetworkImageProvider(attachment.url.toString()),
      ], [
        attachment.description
      ]);
    } else {
      multiImageProvider = MultiImageProvider(
          allMedia!
              .map((e) => CachedNetworkImageProvider(e.url.toString()))
              .toList(),
          allMedia!.map((e) => e.description).toList(),
          initialIndex: allMedia!.indexWhere((e) => e.url == attachment.url));
    }

    showImageViewerPager(
      context,
      multiImageProvider,
      swipeDismissible: true,
      doubleTapZoomable: true,
      immersive: false,
      useSafeArea: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    double? width = AppThemes.isColumnMode(context)
        ? AppThemes.columnWidth * 2
        : MediaQuery.of(context).size.width;
    final thumbnailOnly = attachment.type != MediaType.image;
    final metaInfo = thumbnailOnly
        ? attachment.imageMeta.small ?? attachment.imageMeta.original
        : attachment.imageMeta.original;
    double height =
        metaInfo?.aspect == null ? width : width / metaInfo!.aspect!;
    if (heightLimitImage && height > MediaQuery.of(context).size.height / 2) {
      height = MediaQuery.of(context).size.height / 2;
      width = null;
    }
    return InkWell(
      onTap: () => onOpen(context),
      child: CachedNetworkImage(
        imageUrl: thumbnailOnly
            ? attachment.previewUrl.toString()
            : attachment.url.toString(),
        placeholder: (_, __) =>
            const Center(child: CircularProgressIndicator()),
        errorWidget: (_, __, ___) =>
            BlurHash(hash: attachment.blurhash ?? AppConfigs.fallbackBlurHash),
        width: width,
        height: height,
        fit: fit ?? BoxFit.contain,
      ),
    );
  }
}

class _PlayInBrowserButton extends StatelessWidget {
  final MediaAttachment attachment;
  const _PlayInBrowserButton({
    required this.attachment,
  });

  @override
  Widget build(BuildContext context) {
    double? width = AppThemes.isColumnMode(context)
        ? AppThemes.columnWidth * 2
        : MediaQuery.of(context).size.width;
    final metaInfo =
        attachment.imageMeta.small ?? attachment.imageMeta.original;
    double height =
        metaInfo?.aspect == null ? width : width / metaInfo!.aspect!;
    if (height > MediaQuery.of(context).size.height / 2) {
      height = MediaQuery.of(context).size.height / 2;
      width = null;
    }
    return SizedBox(
      width: width,
      height: height,
      child: Stack(
        children: [
          _AttachmentImageViewer(
            attachment: attachment,
          ),
          Center(
            child: FloatingActionButton.extended(
              heroTag: null,
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              foregroundColor: Theme.of(context).primaryColor,
              icon: const Icon(Icons.videocam),
              label: Text(L10n.of(context)!.playInBrowser),
              onPressed: () => linksCallback(
                attachment.url.toString(),
                context,
              ),
            ),
          )
        ],
      ),
    );
  }
}
