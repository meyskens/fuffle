import 'package:flutter/material.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/mastodon/account.dart';
import 'accounts/avatar.dart';

class AccountListTile extends StatelessWidget {
  final Account account;
  const AccountListTile({required this.account, super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => context.push('/user/${account.id}'),
      leading: Avatar(account: account),
      title: Text(account.calcedDisplayname),
      subtitle: Text('@${account.acct}'),
      trailing: const Icon(Icons.arrow_right),
    );
  }
}
