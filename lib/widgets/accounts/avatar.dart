import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';

import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/model/mastodon/account.dart';

class Avatar extends StatelessWidget {
  final Account account;
  final double? radius;
  const Avatar({required this.account, this.radius, super.key});

  @override
  Widget build(BuildContext context) {
    final placeholder = Icon(
      Icons.person,
      color: Theme.of(context).textTheme.bodyLarge?.color,
    );
    return CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: radius,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: CachedNetworkImage(
          imageUrl: Fuffle.of(context).allowAnimatedImages
              ? account.avatar
              : account.avatarStatic,
          fit: BoxFit.fill,
          placeholder: (_, __) => placeholder,
          errorWidget: (_, __, ___) => placeholder,
        ),
      ),
    );
  }
}
