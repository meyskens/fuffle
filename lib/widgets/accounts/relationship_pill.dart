import 'package:flutter/material.dart';

class RelationPill extends StatelessWidget {
  final String label;

  const RelationPill({
    super.key,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5),
      child: Chip(
        label: Text(
          label,
        ),
        padding: EdgeInsets.zero,
        // labelPadding: EdgeInsets.zero,
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: const BorderSide(color: Colors.transparent)),
      ),
    );
  }
}
