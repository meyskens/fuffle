import 'package:flutter/material.dart';

import 'package:collection/collection.dart';
import 'package:go_router/go_router.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/widgets/accounts/avatar.dart';

class AccountSwitcher extends StatefulWidget {
  final FuffleAccount account;

  const AccountSwitcher({
    super.key,
    required this.account,
  });

  @override
  AccountSwitcherController createState() => AccountSwitcherController();
}

class AccountSwitcherController extends State<AccountSwitcher> {
  Widget iconBox({required Widget child}) => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: SizedBox(
          width: 30,
          height: 30,
          child: child,
        ),
      );

  void onSelected(BuildContext context, int i) async {
    if (i >= 0) {
      await Fuffle.of(context).switchToAccountAt(i);
      if (context.mounted) {
        context.push("/account/$i/home");
      }
      return;
    }
    if (i == -1) {
      context.push("/login");
      return;
    }
    if (i == -2) {
      context
          .push("/account/${Fuffle.of(context).currentAccountIndex}/settings");
    }
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
      onSelected: (i) => onSelected(context, i),
      itemBuilder: (_) => [
        ...Fuffle.of(context)
            .accountDB
            .values
            .mapIndexed((index, element) => PopupMenuItem(
                  value: index,
                  child: Row(
                    children: [
                      iconBox(
                          child: Avatar(account: element.mastodonAccount()!)),
                      Text(element.mastodonAccount()!.calcedDisplayname)
                    ],
                  ),
                )),
        PopupMenuItem(
          value: -1,
          child: Row(
            children: [
              iconBox(child: const Icon(Icons.add)),
              const Text("Add Account"),
            ],
          ),
        ),
        PopupMenuItem(
          value: -2,
          child: Row(
            children: [
              iconBox(child: const Icon(Icons.settings)),
              const Text("Settings"),
            ],
          ),
        ),
      ],
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Avatar(account: widget.account.mastodonAccount()!),
      ),
    );
  }
}
