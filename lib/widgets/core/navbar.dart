import 'package:flutter/material.dart';

import 'package:fuffle/config/app_themes.dart';
import 'package:fuffle/config/go_router_path_extension.dart';

class NavItem {
  final String label;
  final String route;
  final IconData icon;
  final IconData iconFilled;

  NavItem(
      {required this.label,
      required this.route,
      required this.icon,
      required this.iconFilled});

  static List<NavItem> defaultItems = [
    NavItem(
        label: "Home",
        route: "/home",
        icon: Icons.home_outlined,
        iconFilled: Icons.home_filled),
    NavItem(
        label: "Mentions",
        route: "/mentions",
        icon: Icons.alternate_email,
        iconFilled: Icons.alternate_email),
    NavItem(
        label: "Search",
        route: "/search",
        icon: Icons.search_outlined,
        iconFilled: Icons.search),
    NavItem(
        label: "Notifications",
        route: "/notifications",
        icon: Icons.notifications_outlined,
        iconFilled: Icons.notifications),
    NavItem(
        label: "Profile",
        route: "/me",
        icon: Icons.person_2_outlined,
        iconFilled: Icons.person_2)
  ];

  static int indexOf(NavItem e) => defaultItems.indexOf(e);
}

class NavScaffold extends StatelessWidget {
  final AppBar? appBar;
  final Widget? body;
  final int? currentIndex;
  final ScrollController? scrollController;
  final Color? backgroundColor;
  final Widget? floatingActionButton;
  final List<NavItem> navItems = NavItem.defaultItems;
  final bool hideNavigation;

  NavScaffold({
    super.key,
    this.currentIndex,
    this.scrollController,
    this.appBar,
    this.body,
    this.backgroundColor,
    this.floatingActionButton,
    this.hideNavigation = false,
  });

  void onTap(int index, BuildContext context) {
    final String route = navItems[index].route;
    context.go(route);
  }

  void onNewPost(BuildContext context) {
    context.push("/compose");
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final scaffold = Scaffold(
        appBar: appBar,
        body: body,
        backgroundColor: backgroundColor,
        floatingActionButton: floatingActionButton ??
            FloatingActionButton(
              onPressed: () => onNewPost(context),
              child: const Icon(Icons.create),
            ),
        bottomNavigationBar: AppThemes.isColumnMode(context) || hideNavigation
            ? null
            : NavigationBar(
                labelBehavior: NavigationDestinationLabelBehavior.alwaysHide,
                height: 60,
                onDestinationSelected: (i) => onTap(i, context),
                selectedIndex: currentIndex ?? 0,
                destinations: navItems
                    .map((e) => NavigationDestination(
                          icon: Icon(
                            e.icon,
                            size: 28,
                          ),
                          selectedIcon: Icon(
                            e.iconFilled,
                            size: 28,
                          ),
                          label: e.label,
                        ))
                    .toList(),
              ),
      );
      if (!AppThemes.isColumnMode(context)) return scaffold;

      // if in "desktop mode" return the full navbar
      return Material(
        color: Theme.of(context).colorScheme.secondary,
        child: Scaffold(
          body: Row(
            children: [
              const Spacer(),
              SizedBox(
                width: AppThemes.mainColumnWidth,
                child: scaffold,
              ),
              Container(width: 1, color: Theme.of(context).dividerColor),
              SizedBox(
                width: AppThemes.columnWidth,
                child: Column(
                    children: navItems
                        .map((e) => ListTile(
                              leading: Icon(
                                (currentIndex ?? 0) == NavItem.indexOf(e)
                                    ? e.iconFilled
                                    : e.icon,
                                size: 28,
                              ),
                              title: Text(e.label),
                              selected:
                                  (currentIndex ?? 0) == NavItem.indexOf(e),
                              onTap: () => onTap(NavItem.indexOf(e), context),
                            ))
                        .toList()),
              ),
              Container(width: 1, color: Theme.of(context).dividerColor),
              const Spacer(),
            ],
          ),
        ),
      );
    });
  }
}
