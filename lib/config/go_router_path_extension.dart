import 'package:flutter/widgets.dart';

import 'package:go_router/go_router.dart';

const pathParameter = 'account';

extension GoRouterPathExtension on String {
  String asGoRouterPath() => ':$this';

  String asMultiClientRoute() =>
      '/account/:$pathParameter$this'.replaceFirst(RegExp(r'/$'), '');
}

extension GoRouterMultiClient on BuildContext {
  RegExp get _clientRegex => RegExp(r'^/account/(\d+)/?');

  void go(String location, {Object? extra}) {
    location = accountifyLocation(location);
    return GoRouter.of(this).go(location, extra: extra);
  }

  void pop([Object? result]) {
    return GoRouter.of(this).pop(result);
  }

  Future<T?> push<T>(String location, {Object? extra}) {
    location = accountifyLocation(location);
    return GoRouter.of(this).push<T>(location, extra: extra);
  }

  String accountifyLocation(String location) {
    if (!_clientRegex.hasMatch(location)) {
      final route = GoRouterState.of(this);
      final arguments = route.pathParameters;

      String? account = arguments['account'];
      // account ??= Fuffle.of(this).currentAccountIndex.toString();
      location = '/account/$account$location';
    }
    return location;
  }
}
