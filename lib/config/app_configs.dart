import 'package:flutter/material.dart';

abstract class AppConfigs {
  static const String applicationName = 'Fuffle';
  static const String applicationWebsite = 'https://gitlab.com/meyskens/fuffle';
  static const double borderRadius = 12.0;

  static const String loginRedirectUri = 'fuffle://login';
  static const Duration defaultTimeout = Duration(seconds: 30);
  static const String privacyUrl = '$applicationWebsite/-/blob/main/PRIVACY.md';
  static const String issueUrl = '$applicationWebsite/issues';
  static const Color primaryColor = Color.fromARGB(255, 255, 213, 253);
  static const String fallbackBlurHash = 'L5H2EC=PM+yV0g-mq.wG9c010J}I';
  static const String pushGatewayUrl = '';
  static const List<MobileApp> mobileApps = [];
}

class MobileApp {
  final String? link;
  final String asset;

  const MobileApp(this.link, this.asset);
}
