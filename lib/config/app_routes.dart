import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';
import 'package:fuffle/pages/compose_controller.dart';
import 'package:fuffle/pages/hashtag.dart';
import 'package:fuffle/pages/home_controller.dart';
import 'package:fuffle/pages/login.dart';
import 'package:fuffle/pages/mentions_controller.dart';
import 'package:fuffle/pages/notifications_controller.dart';
import 'package:fuffle/pages/search.dart';
import 'package:fuffle/pages/settings.dart';
import 'package:fuffle/pages/status_controller.dart';
import 'package:fuffle/pages/status_likes.dart';
import 'package:fuffle/pages/status_shares.dart';
import 'package:fuffle/pages/user.dart';
import 'package:fuffle/pages/views/page_not_found_view.dart';

class AccountRoute extends GoRoute {
  AccountRoute({
    required String path,
    Widget Function(BuildContext, GoRouterState, FuffleAccount)? builder,
    Page Function(BuildContext, GoRouterState, FuffleAccount)? pageBuilder,
  }) : super(
          path: "/account/:account$path",
          builder: builder != null
              ? (context, state) {
                  final accountIndex =
                      int.parse(state.pathParameters['account']!);
                  final account =
                      Fuffle.of(context).accountDB.getAt(accountIndex);
                  if (account != null) {
                    Fuffle.of(context).currentAccountIndex =
                        accountIndex; // override in fuffle in case we went back and changed account
                  }
                  return builder(context, state, account!);
                }
              : null,
          pageBuilder: pageBuilder != null
              ? (context, state) {
                  final accountIndex =
                      int.parse(state.pathParameters['account']!);
                  final account =
                      Fuffle.of(context).accountDB.getAt(accountIndex);
                  if (account != null) {
                    Fuffle.of(context).currentAccountIndex =
                        accountIndex; // override in fuffle in case we went back and changed account
                  }
                  return pageBuilder(context, state, account!);
                }
              : null,
        );
}

final goRouter = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      redirect: (context, state) => Fuffle.of(context).isLoggedIn
          ? "/account/${Fuffle.of(context).currentAccountIndex}/home"
          : '/login',
    ),
    GoRoute(
        path: "/account/:account",
        redirect: (context, state) {
          final accountIndex = int.parse(state.pathParameters['account']!);
          return "/account/$accountIndex/home";
        }),
    GoRoute(
      path: '/login',
      pageBuilder: (context, state) {
        return const CustomTransitionPage(
          child: LoginPage(),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/home',
      builder: (context, state, account) => HomePage(
        account: account,
      ),
    ),
    AccountRoute(
      path: '/mentions',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: MentionsPage(
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/notifications',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: NotificationsPage(
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/search',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: SearchPage(
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/compose',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: ComposePage(
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/settings',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: SettingsPage(
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/tags/:tag',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: HashtagPage(
            hashtag: state.pathParameters['tag']!,
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/user/:id',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: UserPage(
            id: state.pathParameters['id']!,
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/status/:statusId',
      pageBuilder: (context, state, account) {
        final argument = state.extra as Status?;
        return CustomTransitionPage(
          child: StatusPage(
            statusId: state.pathParameters['statusId']!,
            status: argument,
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/status/:statusId/sharedby',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: StatusSharesPage(
            statusId: state.pathParameters['statusId']!,
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    AccountRoute(
      path: '/status/:statusId/likedby',
      pageBuilder: (context, state, account) {
        return CustomTransitionPage(
          child: StatusLikesPage(
            statusId: state.pathParameters['statusId']!,
            account: account,
          ),
          transitionsBuilder: pushLeft,
        );
      },
    ),
    GoRoute(
        path: '/account/:account/me',
        redirect: (context, state) {
          final accountParamInt = int.parse(state.pathParameters['account']!);
          final account = Fuffle.of(context).accountDB.getAt(accountParamInt);

          return '/account/$accountParamInt/user/${account!.mastodonAccount()!.id}';
        }),
    GoRoute(
      path: '/404',
      builder: (context, state) => const PageNotFoundRouteView(),
    ),
  ],
  errorBuilder: (context, state) => const PageNotFoundRouteView(),
);

SlideTransition pushLeft(context, animation, secondaryAnimation, child) {
  const begin = Offset(1.0, 0.0);
  const end = Offset.zero;
  const curve = Curves.ease;

  var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

  return SlideTransition(
    position: animation.drive(tween),
    child: child,
  );
}
