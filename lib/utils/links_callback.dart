import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher_string.dart';

import 'package:fuffle/config/go_router_path_extension.dart';
import 'package:fuffle/model/fuffle/fuffle.dart';

void linksCallback(String link, BuildContext context) {
  final uri = Uri.parse(link);
  if (uri.pathSegments.length >= 2 &&
      {'tag', 'tags'}.contains(uri.pathSegments[uri.pathSegments.length - 2])) {
    context.push('/tags/${uri.pathSegments.last}');
    return;
  }
  launchUrlString(
    link,
    mode: Fuffle.of(context).useInAppBrowser
        ? LaunchMode.inAppWebView
        : LaunchMode.externalApplication,
  );
}
