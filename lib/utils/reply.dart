import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/pages/compose_controller.dart';
import '../model/database/mastodon/status.dart';

void openReplyCompose(
    BuildContext context, FuffleAccount ownAccount, Status contentStatus) {
  final otherUsers = extractMentionedUsers(contentStatus);
  otherUsers.remove(contentStatus.account!.acct);
  otherUsers.remove(ownAccount.mastodonAccount()!.acct);
  if (contentStatus.account!.acct != ownAccount.mastodonAccount()!.acct) {
    otherUsers.insert(0, contentStatus.account!.acct);
  }

  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (_) => ComposePage(
        account: ownAccount,
        inReplyTo: contentStatus,
        defaultVisibility: contentStatus.visibility,
        atUsers: otherUsers,
        contentWarning: contentStatus.spoilerText,
      ),
    ),
  );
}

List<String> extractMentionedUsers(Status status) {
  List<String> mentionedUsers = [];
  for (var mention in status.mentions!) {
    mentionedUsers.add(mention.acct);
  }
  return mentionedUsers;
}
