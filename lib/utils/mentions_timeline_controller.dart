import 'dart:async';

import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/notification.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';
import 'package:fuffle/utils/status_timeline_controller.dart';
import '../model/database/mastodon/status.dart';

class MentionsTimelineController extends StatusTimelineController {
  final FuffleAccount account;

  MentionsTimelineController(BuildContext context,
      {required super.setState, required this.account})
      : super(context, account, "mentions");

  Future<List<Status>> _requestMentions({
    String? sinceId,
    String? limit,
  }) async {
    String? sinceID;
    if (timeline.isNotEmpty) {
      sinceID = timeline.first.fromNotificationID;
      if (sinceID == null) {
        throw Exception(
            "Cannot fetch more mentions without a fromNotificationID");
      }
    }
    final notifications = await FuffleAPI(account).getNotifications(
      sinceId: sinceID,
      limit: limit,
      filter: [NotificationType.mention],
    );

    notifications.chunk.removeWhere((element) => element.status == null);

    final List<Status> mentions = [];
    for (final notification in notifications.chunk) {
      mentions
          .add(notification.status!.withFromNotificationID(notification.id));
    }

    return mentions.toList();
  }

  Future<int> fetch() async {
    return super.fetchData(_requestMentions);
  }
}
