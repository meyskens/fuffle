import 'package:flutter/material.dart';

import 'package:flutter_list_view/flutter_list_view.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/database/mastodon/notification.dart';
import 'package:fuffle/model/database/timeline.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';

class NotificationsTimelineController
    extends PositionedTimeline<MastodonNotification> {
  final FuffleAccount account;
  Function setState;
  bool _loading = false;

  List<MastodonNotification> uiTimeline = [];

  FlutterListViewController listViewController = FlutterListViewController();

  NotificationsTimelineController(BuildContext context,
      {required this.setState, required this.account})
      : super(context, account, 'notifications');

  Future<int> fetch() async {
    await super.updateMutex.acquire();

    String? firstPostId;
    if (super.length > 0) {
      firstPostId = super.first.id;
    }

    final resp = await FuffleAPI(account).getNotifications(
      sinceId: firstPostId,
      limit: "40",
    );

    if (resp.chunk.isNotEmpty) {
      if (readIndex == -1) {
        // this is probably the first time we load!
        markRead(0); // marks all posts as read
      }
      for (var s in resp.chunk) {
        s.status?.preloadImages();
      }
      super.addList(resp.chunk);
    }

    super.updateMutex.release();
    return resp.chunk.length;
  }

  void removeID(String id) {
    super.deleteWhere((s) => s.id == id);
  }

  void dispose() {
    listViewController.dispose();
  }

  void scrollTop() => listViewController.animateTo(
        0,
        duration: const Duration(milliseconds: 200),
        curve: Curves.ease,
      );

  @override
  void markRead(int i) {
    if (_loading) return;
    final oldIndex = readIndex;
    super.markRead(i);
    if (oldIndex != readIndex) {
      setState(() {});
    }
  }

  void controllerTLtoUITL() {
    _loading = true;
    // check if the use is currently scrolling
    final List<MastodonNotification> newTimeline = [];
    final uiIDs = uiTimeline.map((e) => e.id);
    for (MastodonNotification nt
        in timeline.where((e) => !uiIDs.contains(e.id))) {
      newTimeline.add(nt.restoreFullDataFromJSON());
    }

    uiTimeline.insertAll(0, newTimeline);
    setState(() {});
    _loading = false;
  }
}
