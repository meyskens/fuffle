import '../model/mastodon/account.dart';

String displayName(Account account) {
  return account.displayName.isNotEmpty
      ? account.displayName
      : account.username;
}
