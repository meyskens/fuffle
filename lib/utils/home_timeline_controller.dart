import 'package:flutter/material.dart';

import 'package:fuffle/model/database/account.dart';
import 'package:fuffle/model/fuffle/api/api.dart';
import 'package:fuffle/model/fuffle/api/api_extension.dart';
import 'package:fuffle/utils/status_timeline_controller.dart';

class HomeTimelineController extends StatusTimelineController {
  final FuffleAccount account;

  HomeTimelineController(BuildContext context,
      {required super.setState, required this.account})
      : super(context, account, "home");

  Future<int> fetch() {
    return super.fetchData(FuffleAPI(account).requestHomeTimeline);
  }
}
