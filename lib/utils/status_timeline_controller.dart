import 'package:flutter/material.dart';

import 'package:flutter_list_view/flutter_list_view.dart';

import 'package:fuffle/model/database/mastodon/status.dart';
import 'package:fuffle/model/database/timeline.dart';

class StatusTimelineController extends PositionedTimeline<Status> {
  Function setState;

  List<Status> uiTimeline = [];
  bool _loading = false;

  FlutterListViewController listViewController = FlutterListViewController();

  StatusTimelineController(super.context, super.account, super.name,
      {required this.setState});

  Future<int> fetchData(Function fetcher) async {
    await super.updateMutex.acquire();
    String? firstPostId;
    if (super.length > 0) {
      firstPostId = super.first.id;
    }

    List<Status> timeline = await fetcher(
      sinceId: firstPostId,
      limit: "40",
    );

    if (timeline.isNotEmpty) {
      if (readIndex == -1) {
        // this is probably the first time we load!
        markRead(0); // marks all posts as read
      }
      for (var s in timeline) {
        s.preloadImages();
      }
      super.addList(timeline);
    }

    super.updateMutex.release();
    return timeline.length;
  }

  void removeID(String statusID) {
    super.deleteWhere((s) => s.id == statusID);
  }

  void updateStatus(String statusID, Status newStatus) {
    replaceWhere(
        newStatus,
        (s) =>
            s.id == statusID ||
            s.id == newStatus.id ||
            s.reblog?.id == newStatus.id);
  }

  void dispose() {
    listViewController.dispose();
    //observerController.controller?.dispose();
    // scrollController.dispose();
  }

  void scrollToTop() => listViewController.animateTo(
        0,
        duration: const Duration(milliseconds: 200),
        curve: Curves.ease,
      );

  @override
  void markRead(int i) {
    if (_loading) return;
    final oldIndex = readIndex;
    super.markRead(i);
    if (oldIndex != readIndex) {
      setState(() {});
    }
  }

  void controllerTLtoUITL() {
    _loading = true;
    // check if the use is currently scrolling
    final List<Status> newTimeline = [];
    final uiIDs = uiTimeline.map((e) => e.id);
    for (Status status in timeline.where((e) => !uiIDs.contains(e.id))) {
      newTimeline.add(status.restoreFullDataFromJSON());
    }

    uiTimeline.insertAll(0, newTimeline);
    setState(() {});
    _loading = false;
  }
}
